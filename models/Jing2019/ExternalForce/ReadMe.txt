Four experiment raw data:

Yang-0323_pose.txt;  subject stands with the dumbbell in right hand, 
Yang-0323_motion.txt;
Yang-0323_jump2.txt;
Jump.txt;

1. Open the txt with Matlab: 
	load Jump.txt
2. Jump(1,1) is the first data in the txt file.
3. The sampling rate is 1000Hz;
4. Ten columns:
	frame; time; Fx; Fy; Fz; Torque_x; Troque_y; Troque_z; location of force center_x; location of force center_y
5.Yang_0323_pose.txt:
	T posture with dumbbell on the right hand
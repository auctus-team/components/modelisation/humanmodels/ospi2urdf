clc;
clear all;
load Yang-0323_pose.txt
pose = Yang_0323_pose;
%%% Plot the movement of Cop %%%%
x=0;
y=0;
p = plot(x,y,'*');
axis([-0.3 0.3 -0.3 0.3]);
grid on;
for i=1:1818
    x=jump(i,9); 
    y=jump(i,10); 
    set(p,'XData',x,'YData',y)
    axis([-0.3 0.3 -0.3 0.3]);    
    drawnow
end

%%% Plot Cop %%%%
% load Jump.txt;
jump = resample(Jump, 18*60, 18182)
jump = jump(31:150,:); %the first 2s%
xlswrite('jump1',jump); 
Cox=jump(:,9);
Coy=jump(:,10);
plot(Cox,Coy)

figure(3)
for i = 1:6
subplot(2,3,i)
plot(Cox((i-1)*+1:i*25),Coy((i-1)*25+1:i*25));
axis([-0.3 0.3 -0.3 0.3]);
end

figure(5)
subplot(3,1,1);
plot(jump(:,5))
subplot(3,1,2);
plot(jump(:,9))
subplot(3,1,3);
plot(jump(:,10))


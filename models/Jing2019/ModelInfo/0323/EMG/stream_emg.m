function data_emg = stream_emg(streaming_duration)
% function emg_streaming(steaming_duration) where streaming_duration is
% streaming duration in seconds

if nargin == 0 
    streaming_duration =30;% seconds
end

%% Myo mex header
[mm,m1] = init_myomex();

cleanup_myo = onCleanup(@()cleanUpFun(mm));

%% Creating EMG buffer and figure
fs = 200; % Sampling rate of the armband (hardware-fixed)
n_chan = 8; %Number of emg channels (hardware-fixed)

clear data_emg
global data_emg;
data_emg = [];

animatin_win_len = 3*fs; %Length of animation window (how much signal we see in each frame) in samples
win_updt_step = 0.1*fs; %Step of redrawing the animation window

emg_buf = zeros(animatin_win_len, n_chan); %Buffer that contains emg to draw
time_buf = zeros(animatin_win_len, 1); %Buffer of timestamps (timestamps from armband may be not so regular)

% Animation window
hf = figure(1);
set(hf, 'Position', [300, 400, 1200, 500]);
graph_divider = repmat(1:n_chan,animatin_win_len,1);

%% Main cycle
current_t = 0; %Time passed since start of acquisition in samples

m1.clearLogs();
while current_t < streaming_duration
    %Wait until new data is acquired
    pause_len = win_updt_step/fs;
    pause(pause_len); 
    current_t = current_t+pause_len;
    
    %Pulling new data from MYO
    emg_batch = m1.emg_log;
    time_batch = m1.timeEMG_log;
    m1.clearLogs();
    data_emg = [data_emg; emg_batch];
    
    %Storing new data in buffers
    batch_len = size(emg_batch,1);
    emg_buf = [emg_buf(batch_len+1:end,:); emg_batch];
    time_buf = [time_buf(batch_len+1:end); time_batch];

    %Plotting data
    plot(emg_buf+graph_divider);

    fprintf('%2.1d of %2.1d seconds passed\n',current_t, streaming_duration);
end

1.0323-yang-pose-13-25s.mat, 0323-yang-motion-0-36s.mat :

	channel 4 of the two database is the record of right Biceps, during the T posture with dumbbell in right hand.

2. 0323-yang-jump2-5-15s.mat, 0323-yang-jump1-2-13s.mat :
	
	channel 4 of the two database is the record of right tibia post, during the high knee jump without any external load.

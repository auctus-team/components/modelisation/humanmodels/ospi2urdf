#!/bin/bash
echo conversion fichiers vtp to stl

PATH2VTP="/home/auctus/opensim_optitrack/OpenSim-Gui-Install/Geometry"
PATH2STL="/home/auctus/ospi/models/tst_arm"

mkdir $PATH2STL"/stl"
source ~/vmtk/build/Install/vmtk_env.sh
cd $PATH2VTP
for i in *.vtp
	do
		vmtksurfacewriter -ifile $(basename $i .${i##*.}).vtp -ofile $PATH2STL"/stl/"$(basename $i .${i##*.})".stl"
	done


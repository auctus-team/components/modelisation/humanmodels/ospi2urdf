#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 10:33:53 2020

@author: auctus
"""


import vtk

import sys
import os

absdirname =  os.path.dirname(os.path.abspath(__file__) )

sys.path.append(absdirname + '/../ospi')

import numpy as np
import pinocchio as se3
import os 
import utils

import stl
from stl import mesh

import math

import algebra as alg

import xml.etree.ElementTree as xml

import subprocess

import shutil

class Osim2URDF():
    """!
    Transform a .osim file into a .urdf file.
    """
    
    def __init__(self,osim_path,geom_files_props,filename, ros_package_path, exactly_visual, exactly_collision, correc_stl_dic, verbose=True, secure_collision=True, trans_plugin=True, xacro=True, type_stl_conversion = 1, free_flyer = False, add_rviz = True, roslaunch_name = "",ident_size = 4):
        """!
        Constructor of the Osim2URDF class.
        

        @param osim_path : String.
            Path to the folder containing the .osim file. It should not end by a "/".
            
        @param geom_files_props : [geom_path, geom_type]
            Properties of geometry files.
            geom_path : path to geometry files of the model
            geom_type : type of files ("stl","vtp")
            If those files are .vtp files :
             * set geom_type as "vtp"
             * .vtp files will then be converted into .stl files. It should not end by a "/".
            If those files are .stl files : 
             * set geom_type as ".stl"
             * .stl files will be loaded from this folder
             (Other poss (to implement) : create a shortcut : ln -s $PWD/stl/ ../[osim_model_folder]/stl)

            

        @param filename : String.
            Name to the .osim file. Same name will be used for the URDF file.
                
        @param ros_package_path : String.
            Path to the ROS package associated with the control of the URDF model.
            It should not end by a "/".
            Intervening directly in the ROS package during the realization of the URDF 
            is necessary for the use of the joint_state_publisher package, in order to avoid 
            having to handwrite all the joints in the URDF.

        @param exactly_visual : Boolean.
        
            True : try to represent the visual aspect of the model from the .stl files.
            
            False : use approximations according to the dimensions of the .stl file. 
            The name of the output .urdf file will then be modified as follows : 
            "osim_filename" + "_aVis" + (possibly "_aCol", depending of exactly_collision
            parameter) + (possibly "_sCol", depending of secure_collision
            parameter) + .urdf
            

        @param exactly_collision : Boolean.
        
            True : try to represent the collision aspect of the model from the .stl files.
            
            False : use approximations according to the dimensions of the .stl file.
            The name of the output .urdf file will then be modified as follows : 
            "osim_filename" + (possibly "_aVis", depending of exactly_visual
            parameter)+ "_aCol" + (possibly "_sCol", depending of secure_collision
            parameter) + .urdf
            
        @param correc_stl_dic: Dictionary of dictionnaries.
        
            Sometimes, the stl files could have some mistakes into them, especially
            about the position of their mass center. In order to fix those mistakes,
            the user can add in this dictionary the names of the stl files or body parts 
            causing problems, as well as their true position depending on whether we are 
            dealing with an exact or approximate representation of the human body.
            Then, during the writing of the URDF file, the algorithm will check those
            informations and use them if necessary.
            To sum up, here's a example of an element of this Dictionary :
                
            'name of stl file' : {True_Case : [ coordinates xyz of the mass center of element in case true ],
                                      False_Case : [ coordinates xyz of the mass center of element in case false ]
                                }
                
            'name of body part' : {True_Case : [ coordinates rpy xyz of the origin of all the .stl parts of the
                                                    body part in case true ],
                                       False_Case : [ coordinates rpy xyz of the origin of all the .stl parts of the
                                                    body part in case false ]
                                    }   
                
            With True_Case and False_Case respectively the boolean "True" and "False".
            If no correction is needed in True_Case or False_Case, the list should be empty.
            Ex : 
                
            'stl file bofoot': {True:[],
                                    False:[-0.00004618,  0.025, -0.00304539]
                                },
            'body part fingers_r':{True:[0.,0.,0.,0.,0.,0.], 
                                       False:[]
                                   }    
            
        @param verbose : Boolean.
            Activate the print functions of the algorithm. Useful for debugging.
            
        @param secure_collision : Boolean.
        
            True : in the case the exactly_collision parameter is activated, use an approximation
            of the .stl to secure the collision parameters of the stl file.
            The name of the output .urdf file will then be modified as follows : 
            "osim_filename" + (possibly "_aVis", depending of exactly_visual
            parameter) + (possibly "_aCol", depending of exactly_collision
            parameter) + "_sCol" + .urdf            
            
            False : deactivate this option.
            
        @param trans_plugin : Boolean.
            Activates the writing of the transmission tag and of the .yaml file
            containing the controllers of every joint. 
            
        @param xacro : Boolean.
            Converts the .osim file into a .urdf.xacro file (True) or a .urdf file (False).
        
        @param type_stl_conversion : Int.
        
            Sets the mean of conversion used to convert .vtp files to .stl files.
            
            0 : uses vmtk software to convert files.
            
            1 : uses vtk library. (is able to convert any files, but those files may not look pretty).
            
            The default is 1.
            
        @param free_flyer : Boolean.
        
            Activates or not the gestion of 6 DOF joint (ex : ground_joint).
            
            True : instead of writing 6 joints with one DOF, a unique joint with 6 DOF is written.
            This joint is of type Free Flyer.
            
            False : 6 joints with one DOF are written.

        @param ident_size : Int.
            Allows to set the indentation size in the realized files (.urdf, .yaml, and .launch)
            
        @param add_rviz
        
        @param roslaunch_name
        
        @param stl_path

        @return None.
        """
        
        # set the booleans
        
        ### True : in the case the exactly_collision parameter is activated, use an approximation
        ### of the .stl to secure the collision parameters of the stl file.
        ### False : deactivate this option.
        self.secure_collision = secure_collision
        ### True : try to represent the visual aspect of the model from the .stl files.
        ### False : use approximations according to the dimensions of the .stl file.
        self.exactly_visual = exactly_visual
        ### True : try to represent the collision aspect of the model from the .stl files.
        ### False : use approximations according to the dimensions of the .stl file.
        self.exactly_collision = exactly_collision    
        ### Activates the writing of the transmission tag and of the .yaml file
        ### containing the controllers of every joint. 
        self.trans_plugin = trans_plugin
        ### Activate the print functions of the algorithm. Useful for debugging.
        self.verbose = verbose
        ### Write a .urdf.xacro file or a .urdf file.
        self.xacro = xacro
        ### Define the stl conversion mean that should be used.
        self.type_stl_conversion = type_stl_conversion
        ### Activates or not the gestion of 6 DOF joint (ex : ground_joint).
        self.free_flyer = free_flyer
        # set the path
        
        while osim_path[-1] == "/":
            osim_path = osim_path[:-1]
            
        ### Path to the .osim file.
        self.osim_path = osim_path
        
        while ros_package_path[-1] == "/":
            ros_package_path = ros_package_path[:-1]
        
        ### Path to the ROS package (where the .urdf file will be saved).
        self.package_path = ros_package_path
        ### Path to the .vtp files.

        geom_path = geom_files_props[0]
        geom_type = geom_files_props[1]
        vtp_path = ""
        stl_path = ""

        if (geom_type == "vtp"):
            vtp_path = geom_path
            stl_path = ""
        elif geom_type == "stl":
            stl_path = geom_path
            while stl_path[-1] == "/":
                stl_path = stl_path[:-1]            
            vtp_path = ""
        
        if len(vtp_path.split() ) >1:
            self.vtp_path = '"' + vtp_path + '"'
        else:
            self.vtp_path = vtp_path 
        ### Name of the .osim file
        self.osim_filename = filename
        
        ### Name of the .osim file.
        self.model_name = self.osim_filename.split(".")[0]
        ### Path to the .urdf file.
        self.urdf_path = self.package_path+"/description/"+ self.model_name  + "/"     

        # set useful variables
        
        # correction Dictionary
        
        ### Used to correct the positions of the .stl files.
        self.correc_stl_dic = correc_stl_dic        
        
        # names
        
        ### Name of the ROS package.
        self.package_name = self.package_path.split("/")[-1]     
        ### Path to the static part of the .urdf file.
        # print('basename:    ', os.path.basename(__file__))
        # print('dirname:     ', os.path.dirname(__file__))
        # print('abspath:     ', os.path.abspath(__file__))
        # print('abs dirname: ', os.path.dirname(os.path.abspath(__file__)))
        
        absdirname =  os.path.dirname(os.path.abspath(__file__) )

        self.pwd_begin_launchfile = absdirname +  "/../utilis_files/begin_launcher.txt"
        ### Name of the launchfile of the .urdf file.
        
        if roslaunch_name == "":
            self.launchfile_name = self.package_name + ".launch"
        else:
            self.launchfile_name = roslaunch_name + ".launch"
            
        ### Add rviz node to launchfile
        self.add_rviz = add_rviz
        
        options = ""
        if not self.exactly_visual:
            options+="_aVis"
        if not self.exactly_collision:
            options+="_aCol"
        if self.exactly_collision and self.secure_collision:
            options+="_sCol"
            
        ### Name of the .urdf file, according of the options chosen by the user.
        self.urdf_filename = ""

        if self.xacro:
            self.urdf_filename = self.model_name + options+ ".urdf.xacro"
        else:
            self.urdf_filename = self.model_name + options+ ".urdf"

        # set of strings
        
        ### Used to write all the controllers names into the launchfile.
        self.position_controller = ""
        ### Used to write specific elements at the end of the launchfile.
        self.launchfile_add = []
        
        # indentation variables
                        
        ### Variable counting the indentation level of the urdf file.
        self.ident =0
        ### Number of space for one indentation level.
        self.ident_size = ident_size
        ### Variable counting the indentation level of the transmission file.        
        self.ident_trans = 0
        
        # fake links infos
        
        ### Mass of a fake link
        self.fake_mass = 0.0001
        ### Inertia of a fake link
        self.fake_in= 0.0003

        # Define global variables, useful for the transform osim -> urdf
        
        self.ground_type = None
        
        ### Dictionary containing every useful informations about the .osim
        ### file, in order to convert this file into a .urdf format.
        self.pymodel = readOsim(self.osim_path+"/"+self.osim_filename)

        ### Transformation matrix from the .osim referential to the .urdf referential.
        ### Corresponds to np.array([[0.,0.,1.],[1.,0.,0.],[0.,1.,0.] ]).z_rot
        self.osMpi = np.dot(se3.utils.rotate('z', np.pi/2) , se3.utils.rotate('x', np.pi/2) ) 
        ### The osMpi matrix expressed as Euler angles.
        self.osMpi_rpy = alg.euler_from_rot_mat(self.osMpi)

        ### Contains informations about each of the joints [index,name,axis expressed into the .urdf referential]
        self.joint_models = []
        ### Contains the transformations of each joints, expressed into the .urdf referential.
        self.joint_transformations = []
        test = True
        if test :
            self.joint_models, self.joint_transformations = utils._parse2PinocchioJoints(self.pymodel,self.osMpi, self.free_flyer)      
        else:
            self.joint_models, self.joint_transformations = utils._parse2PinocchioJoints_ori(self.pymodel)      
        
        # Define variables useful for the parsing

        ### List containing all the different transformations that could be linked
        ### with a rotation transform.
        self.L_rot = ["JointModelSpherical", "JointModelRX", "JointModelRY", "JointModelRZ", "JointModelRevoluteUnaligned", "JointModelComposite" ]
        
        ### List containing the names of the parent bodies of each joint.
        self.idx = []
        for joint in range(0,len(self.pymodel['Joints'])):
            parent_name = self.pymodel['Joints'][joint][0]['parent_body'][0]
            self.idx.append(parent_name)
            
        ### Dictionary containing the center of mass of each body part.
        self.dic_body = {}
        ### List containing the names of each body part.
        self.bpart = []
        for body in self.pymodel['Bodies']:
            body_name = body['name'][0]
            self.bpart.append(body_name)
            self.dic_body[body_name] = []

        # create a Matrix containing the maximal effort between each
        # body parts (beta)
        
        ### Matrix containing the maximal efforts between each body part.
        self.M_forces = -1*np.ones(  ( len(self.bpart), len(self.bpart) ) )
        
        self.checkMuscularEffort()

        # then, set the minimum in case we do not have efforts informations
        # for two body parts that are linked by a joint.

        mini = np.inf
        for i in range(len(self.bpart)):
            for j in range( len(self.bpart) ):
                if self.M_forces[i,j] != -1 and self.M_forces[i,j] < mini:
                    mini = self.M_forces[i,j]
        
        ### Approximation of the effort between two body parts 
        ### (in case where the two bodies parts are directly linked by a joint,
        ### but there is no information about the effort between those body parts
        ### into the .osim file).
        self.approx_effort = mini
        ### Approximation of the velocity of a joint.
        self.approx_velocity = 1.0
        
        # Let's go! 
        # First, create the folder where the urdf file will be.
       
        try:
            os.mkdir(self.package_path+"/description/")
        except OSError:
            print("Folder"+self.package_path+"/description" +" already existing.")        
        try:
            os.mkdir(self.package_path+"/description/"+self.model_name)
        except OSError:
            print("Folder"+self.package_path+"/description/"+ self.model_name +" already existing.") 
       

        # transformation : turn joint_transformations into urdf coordinate system
        
        k = 0
        while test==False and k <( len( self.joint_transformations ) ) :

            joint_axis = self.joint_transformations[k]
            joint_axis_urdf = joint_axis
            if joint_axis_urdf.shape[1] ==0:
                k+=1

            else:
                #print("Bef : ", joint_axis_urdf)
                joint_axis_urdf = np.dot( joint_axis_urdf, self.osMpi.T )
                #print("Aft : ", joint_axis_urdf)
                self.joint_transformations[k] = joint_axis_urdf
                k+=1
        
        # fix an issue of the .osim parser (adding fake 'Visuals' objects, that
        # are not linked to any .stl file)  
        
        # k = 0
        # while k < (len(self.pymodel['Visuals'])):
        #     #print(self.pymodel['Visuals'][k][1])
        #     if len(self.pymodel['Visuals'][k][1 ]['geometry_file']) ==0:
        #         del(self.pymodel['Visuals'][k])
        #     else:
        #         k+=1

        # if len(self.pymodel['Visuals'])!=len(self.pymodel['Bodies']):
        #     self.pymodel['Visuals'] = [None]+ self.pymodel['Visuals']          

        # check that the .osim file has all the range infos that are necessary
        
        k = 0

        while k < len(self.pymodel['Joints']):
            
            joint_range = self.pymodel['Joints'][k][1]['range']
            joint_dim = self.joint_models[k][2].nv
            if len(joint_range) != joint_dim:
                print(joint_range)
                print(self.joint_models[k])
            k+=1
        
        # print( len(vtp_path.split()) )
        # print(self.vtp_path)

        #create the .stl files
        
        #print(self.pymodel['Visuals'])
        
        ### Path to the .stl files.
        if (stl_path == ""):
            self.stl_path = self.urdf_path+"stl"
            self.stl_path_from_package = "/" + self.package_name + "/description/"+self.model_name+'/stl/'
        else:
            self.stl_path = stl_path
            cut_path = self.stl_path.split("/")
            i = cut_path.index(self.package_name)
            
            self.stl_path_from_package = "/" + cut_path[i]
            for k in range(i+1,len(cut_path)):
                self.stl_path_from_package += "/" + cut_path[k]
            self.stl_path_from_package += "/"
            
        self.createSTLfiles()
        # # #write the .urdf

        self.writeModelToURDF()


    def createSTLfiles(self):
        """!
        Create the .stl files from the .vtp files, and save them at the
        same location as the .urdf (if those files were not already created).
        This is done thanks to a temporary bash script.
        Warning : this step can take a few minutes!

        @return None.

        """
        
        folder = False

        try:
            os.mkdir(self.stl_path)
        except OSError:
            print("STL files already created. No need to make them another time.")
            folder = True
        
        if not folder and len(self.vtp_path) == 0:
            print("Error : no path to .vtp files given. Perhaps that's because you only have .stl files?")
            print("In that case, put your .stl files into the folder" + self.stl_path + ", and restart the program.")
            exit(1)
            
        if folder :
            cmd = "ls -l " + self.stl_path + "/*.stl |wc -l"
            try : 
                number_stl = int( subprocess.getoutput(cmd) )
            except ValueError:
                number_stl = 0
            
            if number_stl == 0:
                print("Error : no .stl files into the folder " +self.stl_path )
                if len(self.vtp_path) == 0:
                    print("Error : no path to .vtp files given. Perhaps that's because you only have .stl files?")
                    print("In that case, put your .stl files into the folder" + self.stl_path+ ", and restart the program.")
                    exit(1) 

                else:
                    print("Let's retry to write those .stl files.")
                    folder = False
                    shutil.rmtree(self.stl_path,ignore_errors = True)
                    os.mkdir(self.stl_path)
                    
            elif len(self.vtp_path) > 0:
                cmd = 'ls -l ' + self.vtp_path + '/*.vtp |wc -l'
                number_vtp =int( subprocess.getoutput(cmd) )
                try:
                    cmd = 'ls -l ' + self.vtp_path + '/*.obj |wc -l'
                    number_vtp +=int( subprocess.getoutput(cmd) )
                except ValueError:
                    pass
                try:
                    cmd = 'ls -l ' + self.vtp_path + '/*.stl |wc -l'
                    number_vtp +=int( subprocess.getoutput(cmd) )     
                except ValueError:
                    pass                           
                if number_vtp != number_stl:
                    print("Error : not the same number of vtp/stl/obj files (%d) and of stl files (%d)"%(number_vtp,number_stl))
                    print("Let's retry to write those .stl files.")                    
                    folder = False
                    shutil.rmtree(self.stl_path,ignore_errors = True)
                    os.mkdir(self.stl_path)
        if not folder:
            
            with open(self.urdf_path+ "deplace_stl.sh","w+") as f:
                f.write("#!/bin/bash \n\n")
                f.write('PATH2STL1='+self.vtp_path+" \n")
                f.write('PATH2STL2='+self.stl_path+ " \n")
                f.write("cd $PATH2STL1 \n")
                f.write("for i in *.stl \n")
                f.write("    do \n")
                f.write('        cp $(basename $i .${i##*.}).stl $PATH2STL2"/"$(basename $i .${i##*.})".stl" \n')
                f.write("    done \n")               
            os.system("chmod +x "+ self.urdf_path+ "deplace_stl.sh")
            subprocess.call(self.urdf_path+ "deplace_stl.sh")
            os.remove(self.urdf_path+ "deplace_stl.sh")


            with open(self.urdf_path+ "convert_obj2stl.sh","w+") as f:
                f.write("#!/bin/bash \n\n")
                f.write('PATH2OBJ='+self.vtp_path+" \n")
                f.write('PATH2STL='+self.stl_path+ " \n")
                f.write("source ~/vmtk/build/Install/vmtk_env.sh \n")
                f.write("cd $PATH2OBJ \n")
                f.write("for i in *.obj \n")
                f.write("    do \n")
                f.write('        ctmconv $(basename $i .${i##*.}).obj $PATH2STL"/"$(basename $i .${i##*.})".stl" \n')
                f.write("    done \n")               
            os.system("chmod +x "+ self.urdf_path+ "convert_obj2stl.sh")
            subprocess.call(self.urdf_path+ "convert_obj2stl.sh")
            os.remove(self.urdf_path+ "convert_obj2stl.sh")

            if self.type_stl_conversion == 0:

                with open(self.urdf_path+ "convert_vtp2stl.sh","w+") as f:
                    f.write("#!/bin/bash \n\n")
                    f.write('PATH2VTP='+self.vtp_path+" \n")
                    f.write('PATH2STL='+self.stl_path+ " \n")
                    f.write("source ~/vmtk/build/Install/vmtk_env.sh \n")
                    f.write("cd $PATH2VTP \n")
                    f.write("for i in *.vtp \n")
                    f.write("    do \n")
                    f.write('        vmtksurfacewriter -ifile $(basename $i .${i##*.}).vtp -ofile $PATH2STL"/"$(basename $i .${i##*.})".stl" \n')
                    f.write("    done \n")         
                
                os.system("chmod +x "+ self.urdf_path+ "convert_vtp2stl.sh")
                subprocess.call(self.urdf_path+ "convert_vtp2stl.sh")
                os.remove(self.urdf_path+ "convert_vtp2stl.sh")
                
            elif self.type_stl_conversion == 1:
                cmd = "ls -l " + self.vtp_path + "/*.vtp"
                all_vtp = subprocess.getoutput(cmd)
                L_vtp = all_vtp.split("\n")
                
                for k in range(len(L_vtp)):
                    vtp_name = L_vtp[k].split(" ")[-1].split("/")[-1]
                    
                    vtp_path = self.vtp_path + "/" + vtp_name
                    
                    stl_name = vtp_name.split(".")[0] + ".stl"
                    
                    stl_path = self.stl_path+"/" + stl_name
                                        
                    VTP2STL(vtp_path, stl_path)



    def checkMuscularEffort(self):
        """!
        Create a Matrix containing the maximal effort between each
        body parts.
        
        (BETA, as this information may not be useful for Rviz or
        Pinocchio.)
        
        As for now, this is done by realizing an average of the product of the
        optimal_fiber_length and the max_isometric_force of each muscle between
        the concerned body parts.

        @return None.

        """
        
        occ_part = np.zeros( ( len(self.bpart), len(self.bpart) ) )
        for forces in self.pymodel['Forces']:
            ofl = float(forces[0]['optimal_fiber_length'][0])
            mif =float( forces[0]['max_isometric_force'][0])
            val = ofl*mif
            Lbody = []
            for points in forces[1]:
                body_name = points['body'][0]
                if body_name not in Lbody:
                    Lbody.append(body_name)
            #print("Lbody : ", Lbody)
            if len(Lbody) < 2:
                print("error : not a even number of bodies")
            else:
                #print("Lbody : ", Lbody)
                #print(self.bpart)
                for i in range(len(Lbody)):
                    numb_i = self.bpart.index(Lbody[i])
                    for j in range(i+1,len(Lbody)):
                        numb_j = self.bpart.index(Lbody[j])
                        eff_max =  (self.M_forces[numb_i,numb_j]*occ_part[numb_i,numb_j] + ofl*mif)/(occ_part[numb_i,numb_j]+1)
                        occ_part[numb_i,numb_j] += 1
                        occ_part[numb_j,numb_i] += 1
                        
                        # # add to debug
                        #eff_max = eff_max*10
                        
                        self.M_forces[numb_i,numb_j] = eff_max
                        self.M_forces[numb_j,numb_i] = eff_max       
                        

        # verification step : check that every part of body has a force
        for k in range(len(self.bpart)):
            L = [ x for x in range(len(self.bpart)) if self.M_forces[k,x ] !=-1 ]
            if len(L) == 0:
                print("Warning : part %s do not have any muscular links with any other body part."%self.bpart[k])

    def writeModelToURDF(self):
        """!
        
        Write the URDF file.
        Can also write the files useful for the package controller_manager 
        (control.yaml) and for the ros package (.launch).
        
        @return None.

        """
        
        with open(self.urdf_path +self.urdf_filename,"w+") as f: 
            f.write('<?xml version="1.0" encoding="utf-8"?>\n\n')
            
            beg_str = ""
            if self.xacro:
                beg_str= '<robot xmlns:xacro="http://www.ros.org/wiki/xacro" name="' + self.model_name+ '"> \n'
            else:
                beg_str = '<robot name="' + self.model_name+ '"> \n'
                
            f.write(beg_str)
            
            self.ident+= self.ident_size
            
            if self.trans_plugin:
                # write gazebo plugins
                f.write(self.ident*" "+"<gazebo>\n")
                self.ident+= self.ident_size
                f.write(self.ident*" "+'<plugin name="gazebo_ros_control" filename="libgazebo_ros_control.so">\n')
                self.ident+= self.ident_size
                f.write(self.ident*" "+'<robotNamespace>/'+ self.model_name  + '</robotNamespace>\n')
                self.ident-= self.ident_size
                f.write(self.ident*" "+'</plugin>\n')
                self.ident-= self.ident_size
                f.write(self.ident*" "+'</gazebo>\n \n')      
                try:
                    os.mkdir(self.package_path+"/config/")
                except OSError:
                    print("Folder"+self.package_path+"/config" +" already existing.")
                
                try:
                    os.remove(self.package_path+"/config/"+ self.model_name+'_control.yaml')
                except OSError:
                    print("File"+self.package_path+"/config/"+ self.model_name+'_control.yaml' +" is new.")
                
                with open(self.package_path+'/config/'+ self.model_name+'_control.yaml',"a+") as f2:
                    f2.write(self.model_name+":\n\n")
                    self.ident_trans +=self.ident_size
                    f2.write(self.ident_trans*" "+'joint_state_controller:\n')
                    
                    self.ident_trans +=self.ident_size
                    f2.write(self.ident_trans*" "+'type: joint_state_controller/JointStateController\n')
                    f2.write(self.ident_trans*" "+'publish_rate: 50\n\n')            
                    self.ident_trans -=self.ident_size

 
            # first, let's create all the links, for each link
            
            for body in range(0,len(self.pymodel['Bodies'])): # self.pymodel['Joints']
                if self.pymodel['Bodies'][body]['name'][0] !="ground":
                    self.writeLinkVisuals(f,body)            
                else:
                    # we create a special fake link for the ground (or universe)
                    f.write(self.ident*" "+ '<link name="ground" /> \n \n')
                    
                    
            # then, let's write the joints, each joint at a time

            for joint in range(0,len(self.pymodel['Joints'])):
                self.writeJoint(f,joint)          
            

            
            self.ident-= self.ident_size            
            f.write("</robot> \n")

        self.writeLaunchFile()    
        return(None)
    
    
    def writeLaunchFile(self):
        """!
        
        Write the launchfile associated with the URDF file.
        
        NB : A small part of this launchfile is static and written into the 
        "begin_launcher.txt" file, into the folder utilis_files. Should you 
        need to add more nodes to the launchfile of the URDF, you can 
        add them into this file.

        @return None.

        """
        
        with open(self.pwd_begin_launchfile,"r+") as f3:
            beg_content = f3.read()
        
        with open(self.package_path+"/launch/"+self.launchfile_name,"w+") as f:
            f.write(beg_content)
            self.ident+=self.ident_size
            
            str_arg_rviz = '<arg name="activate_rviz" default="'+ str(self.add_rviz)+'"/>\n\n'
            f.write(self.ident*" " + str_arg_rviz)
            str_arg_rviz = '<arg name="use_urdf_model" default="true"/>\n\n'
            f.write(self.ident*" " + str_arg_rviz)


            str_rviz = '<node if="$(arg activate_rviz)" name="rviz" pkg="rviz" type="rviz" args="-d $(find '
            str_rviz += self.package_name + ' )/rviz/human_control.rviz" />\n\n'
            f.write(self.ident*" " + str_rviz)

            f.write(self.ident*" " + '<group if="$(arg use_urdf_model)">\n')
            self.ident+=self.ident_size                

            
            if self.xacro:
                str_robot_desc = '<param name="human_description" command="$(find xacro)/xacro '
                str_robot_desc += self.urdf_path+self.urdf_filename +'"/>\n\n'
                f.write(self.ident*" "+ str_robot_desc)
            
            else:
                str_robot_desc = '<param name="human_description" command="cat '
                str_robot_desc += self.urdf_path+self.urdf_filename +'"/>\n\n'
                f.write(self.ident*" "+ str_robot_desc)
            
            pwd_control = self.package_path+'/config/'+ self.model_name+'_control.yaml'
            f.write(self.ident*" "+ '<rosparam file="'+pwd_control+ '" command="load"/>\n \n')
            
            f.write(self.ident*" "+ '<node name="human_state_publisher" pkg="robot_state_publisher" type="robot_state_publisher" respawn="false" output="screen">\n')
            self.ident+=self.ident_size
            f.write(self.ident*" "+ '<remap from="/joint_states" to="/'+self.model_name+'/joint_states" />\n')
            f.write(self.ident*" "+ '<remap from="robot_description" to="human_description" />\n')
            self.ident-=self.ident_size            
            f.write(self.ident*" "+ '</node>\n\n')
            
            f.write(self.ident*" "+ '<node name="joint_state_publisher_gui" pkg="joint_state_publisher_gui" type="joint_state_publisher_gui" respawn="false" output="screen">\n')
            self.ident+=self.ident_size
            f.write(self.ident*" "+ '<remap from="/joint_states" to="/'+self.model_name+'/joint_states" />\n')
            f.write(self.ident*" "+ '<remap from="robot_description" to="human_description" />\n')
            f.write(self.ident*" "+ '<param name="use_gui" value="true" />\n')
            f.write(self.ident*" "+ '<rosparam param="source_list">["/Q_pub"]</rosparam>\n')
            self.ident-=self.ident_size
            f.write(self.ident*" "+ '</node>\n\n')            
            
            
            str_controller = '<node name="controller_spawner" pkg="controller_manager" type="spawner" respawn="false" output="screen" ns="/'
            str_controller+= self.model_name + '" args="'
            str_controller+= self.position_controller+ ' joint_state_controller"/>\n\n'
            f.write(self.ident*" "+ str_controller)
            
            str_spawn_urdf = '<node name="spawn_urdf" pkg="gazebo_ros" type="spawn_model" args="-file '
            str_spawn_urdf += self.urdf_path+self.urdf_filename + ' -urdf -x 0 -y 0 -z 2 -model '
            str_spawn_urdf += self.model_name + '" />\n\n'
            f.write(self.ident*" "+ str_spawn_urdf)
            
            for string in self.launchfile_add:
                f.write(self.ident*" "+ string)
            
            self.ident-=self.ident_size
            f.write(self.ident*" "+"</group>\n\n")
            self.ident-=self.ident_size
            f.write("</launch>")
            

    def writeJoint(self,f,joint):
        """!
        Write one Joint into the URDF file f.

        @param f : File.
            The URDF file currently written.
        @param joint : Int.
            The number associated with the joint currently being processed.

        @return None

        """
        
        # First, let's get all the necessary informations about our joint
                
        joint_axis_names = []
        joint_axis_types = []
        
        for k in range(len(self.pymodel['Joints'][joint][2]['coordinates'])):
            if self.pymodel['Joints'][joint][2]['coordinates'][k] != None:
                joint_axis_names.append(self.pymodel['Joints'][joint][2]['coordinates'][k])
                joint_axis_types.append(self.pymodel['Joints'][joint][2]['name'][k])
        
        #list(filter(None,self.pymodel['Joints'][joint][2]['coordinates']))
        
        child = self.pymodel['Joints'][joint][0]['child_body'][0]
        joint_name = self.pymodel['Joints'][joint][0]['name'][0] 
        parent = self.pymodel['Joints'][joint][0]['parent_body'][0]       
        joint_model = self.joint_models[joint][2]
        joint_limits = self.pymodel['Joints'][joint][1]['range']

        # From OpenSim to Pinocchio
        joint_placement = se3.SE3.Identity()
        
        r = np.matrix(self.pymodel['Joints'][joint][0]['orientation_in_parent'][0],dtype = np.float64).T
        
        joint_placement.rotation = se3.utils.rpyToMatrix( np.dot(self.osMpi , r) )
                
        t = self.pymodel['Joints'][joint][0]['location_in_parent'][0]   
             
        joint_placement.translation = np.dot( self.osMpi ,  np.matrix(t,dtype=np.float64).T )        
        
        joint_axis = self.joint_transformations[joint]
        
        joint_model_type = str( type(joint_model) ).split(".")[-1][:-2] 
        
        
        if self.verbose:
            print('Joint Name: ', joint_name)
            print('Parent Name: ', parent)
            print("Child name :", child )
            print("Joint axis names : ", joint_axis_names)
            print("Joint axis types : ", joint_axis_types)
            print( 'Joint Model: ', joint_model ) 
            print( 'Joint Limits: ', joint_limits )
            print("Joint orientation (w.r.t parent frame) : ", joint_axis.astype(float) ) #joint_axis[0,1])
            print("Joint origin (w.r.t parent frame): ", joint_placement ) #joint_placement.translation[2]) 
            print("Joint origin rot Euler : ", alg.euler_from_rot_mat(joint_placement.rotation))
            print("Joint Model Type : ", joint_model_type)
            print("Joint Model nv : ", joint_model.nv)
            print("*****")        
        
        # check if the joint is a fixed type         
        if joint_model.nv == 0:
            if parent == "ground":
                self.ground_type = "fixed"
            if self.exactly_visual : 
                #R = self.osMpi
                R = np.eye(3,3)
            else:
                R = np.eye(3,3)
            self.writeSpecificJoint(f,"fixed",joint_name,parent,child, joint_placement.translation,R)
        
        # check if the joint is a rotation type 
        elif joint_model_type in self.L_rot:
            
            if parent == "ground":
                self.ground_type = joint_model_type

            L_j = []

            if joint_model_type == "JointModelComposite":
                obj = self.joint_models[joint][2]
                for a in obj.joints:
                    L_j.append(a)
            else:
                L_j.append(joint_model)

            for k in range(len(L_j)):

                sub_joint_model= L_j[k]
                sub_joint_model_type = str( type(sub_joint_model) ).split(".")[-1][:-2] 

                # print(joint_axis[k,:])
                                
                if sub_joint_model_type != "JointModelRevoluteUnaligned" :
                    # check if the axis should be expressed with int or
                    # float values
                    
                    n_j_ax=joint_axis[k,:].astype(float).astype(int)

                    if np.sum(n_j_ax) == 0 :
                        #bug : fix it 
                        joint_axis[k,:] = joint_axis[k,:].astype(float)
                    else:
                        joint_axis[k,:] = n_j_ax
                

            # check if it only has one DOF
            if joint_model.nv == 1:
                joint_placement_rot = alg.euler_from_rot_mat(joint_placement.rotation)
                joint_axis_name = joint_axis_names[0]
                joint_axis_type = joint_axis_types[0]
                self.writeOneDofJoint(f,joint_axis_name,joint_axis_type,parent,child,joint_limits, joint_axis, joint_placement.translation,joint_placement_rot)
            else:
                self.writeMultipleDimensionJoint(f,joint_axis_names,joint_axis_types,parent,child,joint_limits, joint_axis, joint_placement,joint_model.nv)
        
        elif joint_model.nv==6:
            # means that we are in case JointModelFreeFlyer
            # --> equivalent to floating joint            
            if parent == "ground":
                self.ground_type = "floating"
                
            if self.free_flyer :
                if self.exactly_visual : 
                    #R = self.osMpi
                    R = np.eye(3,3)
                else:
                    R = np.eye(3,3)
                self.writeSpecificJoint(f,"floating",joint_name,parent,child, joint_placement.translation,R)
                
            else:
                # write first translation, then rotation               
                joint_axis_names = joint_axis_names[3:6] + joint_axis_names[0:3] 
                joint_axis_types = joint_axis_types[3:6] + joint_axis_types[0:3] 
                joint_limits = joint_limits[3:6] + joint_limits[0:3]        
                joint_axis = np.vstack( ( joint_axis[3:6,:], joint_axis[0:3,:])   )

                
                self.writeMultipleDimensionJoint(f,joint_axis_names,joint_axis_types,parent,child,joint_limits, joint_axis, joint_placement,joint_model.nv)

                    

        
    def writeSpecificJoint(self,f,joint_type,joint_name,parent,child, joint_placement_trans,R):      
        """!
        Write a specific Joint into URDF file (floating or fixed).
        (Normally, only joint from ground to 1st part of body)
        
        For the floating joint, a special procedure must be followed.
        
        https://github.com/ros/robot_model/issues/188 : according to this,
        there is an issue with floating joints for joint_state_publisher. In order
        to fix this, we'll just add a static transform publisher to the launchfile.
        
        NB : we also write a floating joint into the urdf, just to be sure that the
        joint_state_publisher node won't consider that there is two links without any
        connexion, thus two root links.

        @param f : File.
            The URDF file currently written.
            
        @param joint_type : String.
            The type (floating, fixed) associated with the joint currently being processed.     
            
        @param joint_name : String.
            The name associated with the joint currently being processed.
            
        @param parent : String.
            Name of the parent body part of the joint.
            
        @param child : String.
            Name of the child body part of the joint.
            
        @param joint_placement_trans : Array 1x3.
            Position xyz of the origin of the joint.
        
        @param R : numpy.array (3x3).
            Rotation matrix of the origin of the joint

        @return None.
        
        """

        # into URDF
        
        f.write(self.ident*" "+ '<joint name="' + joint_name + '" type="' + joint_type +'"> \n \n')
        self.ident+=self.ident_size
        
        p = joint_placement_trans
        
        r,pc,y =  alg.euler_from_rot_mat(R)
        
        str_p = "{: .8f} {: .8f} {: .8f}".format( p[0], p[1], p[2] )
        
        str_rpy = "{: .8f} {: .8f} {: .8f}".format( r, pc, y )

        f.write(self.ident*" "+ '<origin xyz="'+ str_p +  '" rpy="'+str_rpy  +'" /> \n')   
        f.write(self.ident*" "+ '<parent link="'+ parent +  '"/> \n')           
        f.write(self.ident*" "+ '<child link="'+ child +  '"/> \n \n')                  
        
        self.ident-=self.ident_size        
        f.write(self.ident*" "+"</joint> \n \n")
        
        # into launchfile
        if parent == "ground" and joint_type == "floating":
            rate = 1
            p = joint_placement_trans
            
            str_p = "{: .8f} {: .8f} {: .8f} {: .8f} {: .8f} {: .8f}".format(p[0], p[1], p[2],r, pc, y )
            
            launch_str = '<node pkg="tf" type="static_transform_publisher" name="%s" '%joint_name
            launch_str+= 'args="%s %s %s %d" />\n\n'%(str_p,parent,child,rate)

            self.launchfile_add.append(launch_str)
           
        
    def writeOneDofJoint(self,f,joint_name,joint_type,parent,child,joint_limits, joint_axis, joint_placement_trans, joint_placement_rot):
        """!
        
        Write one DOF revolute Joint into URDF file

        @param f : File.
            The URDF file currently written.
            
        @param joint_name : String.
            The name associated with the joint currently being processed.
            
        @param joint_type : String.
            The type (rotation or translation) associated with the joint currently being processed.     

        @param parent : String.
            Name of the parent body part of the joint.
            
        @param child : String.
            Name of the child body part of the joint.
            
        @param joint_limits : List ( format : [ ['-1.57'],['1.57'] ] ).
            List containing the angular limits of the joints.
        
        @param joint_axis : Array 3x1.
            The joint axis specified in the joint frame. This is the axis of rotation for the
            revolute joint, specified in the joint frame of reference.
            
        @param joint_placement_trans : Array 1x3.
            Position xyz of the origin of the joint.
            
        @param joint_placement_rot : Array 1x3.
            Orientation rpy of the origin of the joint.

        @return None.

        """
        
        if "rotation" in joint_type:
            f.write(self.ident*" "+ '<joint name="' + joint_name + '" type="revolute"> \n \n')
        elif "translation" in joint_type:
            f.write(self.ident*" "+ '<joint name="' + joint_name + '" type="prismatic"> \n \n')
        self.ident+=self.ident_size
        
        p = joint_placement_trans
        r = joint_placement_rot
        
        str_p = "{: .8f} {: .8f} {: .8f}".format( p[0], p[1], p[2] )
        str_r = "{: .8f} {: .8f} {: .8f}".format( r[0], r[1], r[2] )

        f.write(self.ident*" "+ '<origin xyz="'+ str_p +  '" rpy="'+str_r+'" /> \n')   
        f.write(self.ident*" "+ '<parent link="'+ parent +  '"/> \n')           
        f.write(self.ident*" "+ '<child link="'+ child +  '"/> \n')          

        str_axis = "{: .8f} {: .8f} {: .8f}".format( joint_axis[0,0], joint_axis[0,1], joint_axis[0,2] )
        
        # if (joint_name == "wrist_flex_l"):
        #     print(str_axis)
        #     print(joint_axis)
        
        f.write(self.ident*" "+ '<axis xyz="'+ str_axis +  '"/> \n')      
        
        if child[-3:-1] == "_f":
            child = child[:-3]
        if parent[-3:-1] == "_f":
            parent = parent[:-3]
        
        # write efforts
        
        numb_i = self.bpart.index(parent)
        numb_j = self.bpart.index(child)
        
        if self.M_forces[numb_i,numb_j] !=-1:
            eff = str(self.M_forces[numb_i,numb_j])
        else:
            eff = str(self.approx_effort )
        
        veloc = str(self.approx_velocity)

        f.write(self.ident*" "+ '<limit effort="'+ eff + '" velocity="' + veloc +  '" lower="' + joint_limits[0][0] + '" upper="' + joint_limits[0][1] +  '" /> \n \n')

        self.ident-=self.ident_size        
        f.write(self.ident*" "+"</joint> \n \n")
        
        if self.trans_plugin:
            self.writeTransmission(f,joint_name)
            
    def writeTransmission(self,f,joint_name):
        """!
        
        Write the transmission elements and the controller elements of the joint,
        in order to be used by the ros_control package.

        @param f : File.
            The URDF file currently written.
        @param joint_name : String.
            The name associated with the joint currently being processed.

        @return None.

        """
        
        with open(self.package_path+'/config/'+ self.model_name+'_control.yaml',"a+") as f2:
            # modify control.yaml
            self.position_controller+= joint_name+"_position_controller "
            
            f2.write(self.ident_trans*" "+joint_name+"_position_controller:\n")
            self.ident_trans+=self.ident_size
            f2.write(self.ident_trans*" "+"type: effort_controllers/JointPositionController\n")
            f2.write(self.ident_trans*" "+"joint: "+ joint_name+ "\n")
            f2.write(self.ident_trans*" "+"pid: {p: 100.0, i: 0.01, d: 10.0}\n\n")
            self.ident_trans-=self.ident_size
        
        # write transmission
        f.write(self.ident*" "+ '<transmission name="trans_'+ joint_name+'">\n')
        self.ident+=self.ident_size
        f.write(self.ident*" "+ '<type>transmission_interface/SimpleTransmission</type>\n')
        f.write(self.ident*" "+ '<joint name="'+ joint_name +'">\n')
        self.ident+=self.ident_size
        f.write(self.ident*" "+ '<hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>\n')
        self.ident-=self.ident_size
        f.write(self.ident*" "+ '</joint>\n')
        f.write(self.ident*" "+ '<actuator name="motor_'+ joint_name +'">\n')
        self.ident+=self.ident_size
        f.write(self.ident*" "+ '<hardwareInterface>hardware_interface/EffortJointInterface</hardwareInterface>\n')
        f.write(self.ident*" "+ '<mechanicalReduction>1</mechanicalReduction>\n')        
        self.ident-=self.ident_size
        f.write(self.ident*" "+ '</actuator>\n')        
        self.ident-=self.ident_size
        f.write(self.ident*" "+ '</transmission>\n \n')               
        
    

    def writeMultipleDimensionJoint(self,f,joint_axis_names,joint_axis_types,parent,child,joint_limits, joint_axis, joint_placement,dim):
        """!
        Write a revolute joint with multiple DOF. 
        As the URDF format do not accept to write a joint with multiple DOF and limits
        on its DOF, the trick is to write multiple one DOF joint between the parent
        and the child link (or body part). To do so, some fake links are created between
        the parent joint and the child joint, and each of these fake links has a
        one DOF joint.

        @param f : File.
            The URDF file currently written.
            
        @param joint_axis_names : List of String.
            The list of names of each DOF of the joint.
            
        @param joint_axis_types : List of String.
            The list of the type of each DOF of the joint (translation or rotation).
            
        @param parent : String.
            Name of the parent body part of the joint.
            
        @param child : String.
            Name of the child body part of the joint.
            
        @param joint_limits : List ( format : [ [ ['0.0'],['3.14'] ], [ ['-1.57'],['1.57'] ], ... ]).
            List containing the angular limits of the joints on all its DOF.
        
        @param joint_axis : Array 3xDOF.
            The joint axis specified in the joint frame on each DOF. Those are the axis of rotation for the
            revolute joint, specified in the joint frame of reference.
            
        @param joint_placement : Pinocchio Homogeneous Matrix object (expressed with a
        Position matrix P and a Rotation matrix R).
            Position and rotation of the origin of the joint, expressed into the
            referential of the parent of this joint.
            
        @param dim : Int.
            Number of DOF of the joint.

        @return None.

        """

        pname = parent
        j_p_t = joint_placement.translation
        j_p_r = alg.euler_from_rot_mat(joint_placement.rotation)

        
        for k in range(0,dim):
            
            cur_joint_axis = joint_axis[k].astype(float)
            name_fj = joint_axis_names[k]
            type_fj = joint_axis_types[k]
            j_l = [joint_limits[k] ]

            if k == dim-1:
                cname = child 
            else:
                cname = child + "_f%i"%(k+1)
                self.writeFakeLink(f,cname)
                
            self.writeOneDofJoint(f, name_fj, type_fj, pname, cname, j_l, cur_joint_axis, j_p_t,j_p_r)
            
            pname = cname
            j_p_t = np.zeros( (1,3) ).flatten()
            j_p_r = np.zeros( (1,3) ).flatten()

        
    
    def writeFakeLink(self,f,body_name):
        """!
        In the case of a joint with more than one DOF (in order to
        keep the limits into the URDF file), we create a fake link which will
        be used to express the transform. 
        
        Here, the fake link is written consecutively to the fake joint.         

        @param f : File
            The URDF file currently written.
            
        @param body_name : String
            The name associated with the fake link currently written.

        @return None.


        """

        f.write(self.ident*" "+ '<link name="' + body_name + '"> \n \n')
        self.ident+=self.ident_size
        
        # Time to get infos about the body
        mass = self.fake_mass
        mass_center = np.zeros((3,1))
        inertia_matrix = self.fake_in*np.eye(3,3)
        
        ixx = str(inertia_matrix[0,0])
        iyy = str(inertia_matrix[1,1])
        izz = str(inertia_matrix[2,2])
        ixy = str(inertia_matrix[0,1])
        ixz = str(inertia_matrix[0,2])
        iyz = str(inertia_matrix[1,2])

        # Let's put inertial first
        f.write(self.ident*" "+ "<inertial>\n")
        self.ident+= self.ident_size
    
        f.write(self.ident*" "+ '<mass value="'+ str(mass)+  '"/> \n')

        str_mass_center = "{: .8f} {: .8f} {: .8f}".format( mass_center[0,0], mass_center[1,0], mass_center[2,0] )
        
        f.write(self.ident*" "+ '<origin xyz="'+ str_mass_center+  '" rpy="0 0 0" /> \n')   
        
        f.write(self.ident*" "+ '<inertia ixx="'+ixx+ '" iyy="'+iyy+'" izz="'+izz +'" ixy="' + ixy + '" ixz="' + ixz + '" iyz="' + iyz + '"/> \n')

        self.ident-= self.ident_size                
        f.write(self.ident*" " + "</inertial> \n \n")

        # everything set for this body!
        self.ident-=self.ident_size        
        f.write(self.ident*" "+"</link> \n \n")        
    
    def transformOsMpi_visu(self,r,p,y):
        """!
        
        As the .stl files are expressed into a specific referential, this method
        uses another transform to change from the stl referential to the
        urdf referential.
        (BETA, some bugs may occurs depending on the configuration of
        the .stl file).

        @param r : float. 
            Roll angle ( x-axis ) expressed in the stl referential.
        @param p : float. 
            Pitch angle ( y-axis ) expressed in the stl referential.
        @param y : float. 
            Yaw angle ( z-axis ) expressed in the stl referential.


        @return ru : float. 
            Roll angle ( x-axis ) expressed in the urdf referential.
        @return pu : float. 
            Pitch angle ( y-axis ) expressed in the urdf referential.
        @return yu : float. 
            Yaw angle ( z-axis ) expressed in the urdf referential.

        """

        S = np.array([[0.,0.,1.],[1.,0.,0.],[0.,1.,0.]])
        V = np.eye(3,3)
        
        R_os_roll = alg.euler_matrix(r, 0, 0)[:3,:3]
        R_os_pitch = alg.euler_matrix(0,p,0)[:3,:3]
        R_os_yaw = alg.euler_matrix(0,0,y)[:3,:3]
        
        R_ur = np.dot(S,R_os_roll)
        R_ur = np.dot(R_ur,R_os_pitch)
        R_ur = np.dot(R_ur,R_os_yaw)
        
        ru,pu,yu = alg.euler_from_rot_mat(R_ur)
        
        return(ru,pu,yu)
    

   # def writeMeshFile(self,type_tag,filename,body_name,scale_factors,body,i_mesh,type_name,f):
    def writeMeshFile(self,type_tag,filename,body_name,scale_factors,body,i_mesh,type_name,ros_filename,f):
        """!
        
        Write a .stl file under a tag "type_tag", according to the conditions of the user 
        about this tag (thanks to exactly_visual and exactly_collision).
        
        @param type_tag : String.
            Type of the written tag ("visual" or "collision").
        @param filename : String.
            Complete path to the .stl file.
        @param body_name : String.
            Name of the body object associated with the mesh file.
        @param scale_factors : List 3x1.
            List containing the scale factors of the mesh file.
        @param body : Int.
            The number associated with the body part currently being processed.
        @param i_mesh : Int
            The number associated with the mesh file currently being processed.
        @param type_name : String.
            Name of the .stl file.
        @param ros_filename : String.
            Path to the .stl file, starting from the name of the ROS package.
        @param f : File.
            The URDF file currently written.

        @return None.

        """
        if type_tag == "visual":
            type_boolean = self.exactly_visual
        elif type_tag == "collision":
            type_boolean = self.exactly_collision
        str_scale_factors= "{: .8f} {: .8f} {: .8f}".format( scale_factors[0], scale_factors[1], scale_factors[2] )


        type_obj = []
        carac_obj = []
        
        transform = np.matrix(self.pymodel['Visuals'][body][1]['transform'][i_mesh],dtype=np.float64).T
        
        if self.exactly_collision==False or self.exactly_visual==False or (self.exactly_collision== True and self.secure_collision== True):
            type_obj,carac_obj = self.getSTLApproximation(filename,body_name,scale_factors)
            cur_xyz = self.dic_body[body_name][-1]
        
        if body_name in self.correc_stl_dic and len(self.correc_stl_dic[body_name][type_boolean])>0:
            Ltrans = self.correc_stl_dic[body_name][type_boolean]
            try:
                transform = np.array([Ltrans]).reshape((6,1))
                ro2,po2,yo2 = transform[0,0],transform[1,0],transform[2,0]
            except ValueError:
                cur_xyz = Ltrans
                R_osim = alg.euler_matrix(transform[0,0], transform[1,0], transform[2,0])[:3,:3]
                R_urdf = np.dot( np.dot(self.osMpi, R_osim), self.osMpi.T)
                ro2,po2,yo2 = alg.euler_from_rot_mat(R_urdf)
                
        else:
            
            transform[3:6] = np.dot( self.osMpi , transform[3:6] )
                
            ro,po,yo = transform[0,0], transform[1,0], transform[2,0]
            ru,pu,yu = self.transformOsMpi_visu(transform[0,0], transform[1,0], transform[2,0])

            R_osim = alg.euler_matrix(transform[0,0], transform[1,0], transform[2,0])[:3,:3]
            R_urdf = np.dot( np.dot(self.osMpi, R_osim), self.osMpi.T)
            ro2,po2,yo2 = alg.euler_from_rot_mat(R_urdf)
                
            transform[0,0] = ru
            transform[1,0] = pu
            transform[2,0] = yu

        if type_boolean:

            str_rpy_transform = "{: .8f} {: .8f} {: .8f}".format(
                    transform[0, 0], transform[1, 0], transform[2, 0])
            str_xyz_transform = "{: .8f} {: .8f} {: .8f}".format( transform[3,0], transform[4,0], transform[5,0] )
                
            self.writeExact(type_tag, type_name, ros_filename, f, str_rpy_transform, str_xyz_transform, str_scale_factors)
            if type_tag =="collision" and self.secure_collision:
                    str_xyz_transform = "{: .8f} {: .8f} {: .8f}".format( cur_xyz[0], cur_xyz[1], cur_xyz[2] )
                    str_rpy_transform = "{: .8f} {: .8f} {: .8f}".format( ro,po,yo )
                    self.writeApproximation("collision_checking",type_name, f, str_rpy_transform, str_xyz_transform, str_scale_factors,type_obj,carac_obj)                   
            
        else:
            if (self.verbose): 
                print("File : %s"%filename)
                print("Type object : %s"%type_obj)        
                
            t_1 = np.array([[cur_xyz[0]],[cur_xyz[1]],[cur_xyz[2]]])
            #t2 = np.dot(self.osMpi.T,t_1)
            #t2 = np.dot(R_visu_urdf,t_1)
            t2 = t_1
                
            str_xyz_transform = "{: .8f} {: .8f} {: .8f}".format( t2[0,0], t2[1,0], t2[2,0] )
            str_rpy_transform = "{: .8f} {: .8f} {: .8f}".format( ro2,po2,yo2)
            self.writeApproximation(type_tag,type_name, f, str_rpy_transform, str_xyz_transform, str_scale_factors,type_obj,carac_obj)
        
    
    
    def writeLinkVisuals(self,f,body):
        """!
        
        Write a link with multiple STL files, as the .urdf format supports
        this feature (which avoids having to write a large number of useless 
        links for each .stl file.).

        @param f : File.
            The URDF file currently written.
        @param body : Int.
            The number associated with the body part currently being processed.

        @return None.

        """
        
        body_name =self.pymodel['Bodies'][body]['name'][0]
            
        f.write(self.ident*" "+ '<link name="' + body_name + '"> \n \n')
        self.ident+=self.ident_size
        
        # Time to get infos about the body
        mass = np.float64(self.pymodel['Bodies'][body]['mass'][0])
        mass_center = np.dot( self.osMpi , np.matrix(self.pymodel['Bodies'][body]['mass_center'][0], dtype = np.float64).T )
        inertia_matrix = np.matrix(self.pymodel['Bodies'][body]['inertia'][0], dtype = np.float64)
        
        ixx = str(inertia_matrix[0,0])
        iyy = str(inertia_matrix[1,1])
        izz = str(inertia_matrix[2,2])
        ixy = str(inertia_matrix[0,1])
        ixz = str(inertia_matrix[0,2])
        iyz = str(inertia_matrix[1,2])
                
        # Let's put inertial first
        f.write(self.ident*" "+ "<inertial> \n ")
        self.ident+= self.ident_size
    
        #print(mass)
        f.write(self.ident*" "+ '<mass value="'+ str(mass)+  '"/> \n')

        str_mass_center = "{: .8f} {: .8f} {: .8f}".format( mass_center[0,0], mass_center[1,0], mass_center[2,0] )
        
        f.write(self.ident*" "+ '<origin xyz="'+ str_mass_center+  '" rpy="0 0 0" /> \n')   
        
        f.write(self.ident*" "+ '<inertia ixx="'+ixx+ '" iyy="'+iyy+'" izz="'+izz +'" ixy="' + ixy + '" ixz="' + ixz + '" iyz="' + iyz + '"/> \n')

        self.ident-= self.ident_size                
        f.write(self.ident*" " + "</inertial> \n \n")

        # then, visuals and collisions
        # let's add all available visuals in one link
        try:
            scale_factors_visu = np.matrix(self.pymodel['Visuals'][body][0]['scale_factors'][0], np.float64).T

        except Exception:
            scale_factors_visu = [1.0,1.0,1.0]

    
        # add to visuals list
        
        if self.verbose:
            print("---- Body name : %s ----"%body_name)
            print("Index body : ",body)
            print("Meshes : ", self.pymodel['Visuals'][body][1]['geometry_file'])
            
        
        # condition to check if the current link is a fake link or not.
        # if it is a fake link --> skip this part.
        if len( self.pymodel['Visuals'][body][1]['geometry_file']) !=0 :

            for i_mesh in range(0, len(self.pymodel['Visuals'][body][1]['geometry_file']) ):
                #take infos about the .stl file
                # type_mesh_i = os.path.splitext(self.pymodel['Visuals'][body][1]['geometry_file'][i_mesh])[1]
                # if (type_mesh_i == ".obj"):
                #     type_mesh_o = ".obj"
                # elif type_mesh_i == ".vtp":
                #     type_mesh_o = ".stl"
                    
                visual_name = os.path.splitext(self.pymodel['Visuals'][body][1]['geometry_file'][i_mesh])[0]
    
                filename = self.stl_path+ '/'+visual_name+'.stl'
                
                ros_filename = self.stl_path_from_package +visual_name+'.stl'                       
                
                try:
                    scale_factors_mesh = (np.matrix(self.pymodel['Visuals'][body][1]['scale_factors'][i_mesh], np.float64)).T 
                    scale_factors = np.asarray(scale_factors_mesh.T)[0]                
    
                except Exception:
                    scale_factors = scale_factors_visu

                self.writeMeshFile("visual",filename,body_name,scale_factors,body,i_mesh,visual_name,ros_filename,f)
                
                self.writeMeshFile("collision",filename,body_name,scale_factors,body,i_mesh,visual_name,ros_filename,f)
            
        # everything set for this body!
        self.ident-=self.ident_size        
        f.write(self.ident*" "+"</link> \n \n")

    def getSTLApproximation(self,stl_file,body_name,scale_factors):
        """!
        
        Reads an .stl file and get usefull informations from this file, in order
        to approximate it by a geometric object supported by the .urdf format 
        ( a box, a cylinder, or a sphere).

        @param stl_file : String
            Path to the .stl file currently under review.
            
        @param body_name : String
            Name of the body associated with the .stl file.

        @param scale_factors : numpy array (3,)
            Scale factors associated with the .stl file. Expressed onto the axis
            of the .stl file (x,y,z into the .osim referential).
        
        @return type_obj : String
            Name of the object approximating the .stl file (cylinder, box, or sphere)
            
        @return carac_obj : List of float
            Features of the object approximating the .stl file.
            
            Cylinder : [radius of the cylinder, length of the cylinder ]
            
            Sphere : [radius of the sphere]
            
            Box : [Size of box on the x axis, Size of box on y axis, Size of box on z axis]

        """
        
        main_body = mesh.Mesh.from_file(stl_file)
        
        stl_name = stl_file.split("/")[-1].split(".")[0]
        
        # if (self.verbose):
        #     print("Stl name : ", stl_name)
        #     print(stl_name in self.correc_stl_dic)

        if stl_name in self.correc_stl_dic and len(self.correc_stl_dic[stl_name][False])>0:
            # approximation --> case False : we do not want the correction
            # for the exact case, but for the approximation case
            mass_center_stl = self.correc_stl_dic[stl_name][False]
        else:
            mass_props = main_body.get_mass_properties()
            mass_center_stl = np.dot(self.osMpi, mass_props[1] )
        self.dic_body[body_name].append( mass_center_stl )

        minx, maxx, miny, maxy, minz, maxz = self.find_mins_maxs(main_body)
        
        try : 
            Dx = (maxx - minx)*scale_factors[0]
            Dy = (maxy - miny)*scale_factors[1]
            Dz= (maxz - minz)*scale_factors[2]
        except Exception:
            print("Error with file %s : are you sure of its name? \n"%(stl_file))
            print("If the file is there, it may be corrupted. \n")
            exit(0)
        
        # let's approximate this object
        
        min_order=  math.log10( max(Dx,Dy,Dz) )  -1
        
        if min_order < 0:
            min_order = math.floor(min_order)
        else:
            min_order = math.ceil(min_order)
        
        lim_diff = 4*(10**(min_order))
        
        type_obj= []
        carac_obj = []
        
        if abs(Dx-Dz) < lim_diff and abs(Dy-Dz) < lim_diff and abs(Dx-Dy) < lim_diff:
            type_obj = "sphere"
            radius = np.mean([Dx,Dy,Dz])/2
            carac_obj = [radius]

        
        elif abs(Dx-Dz) < lim_diff:
            type_obj = "cylinder"
            radius = np.mean([Dx,Dz])/2
            length = Dy
            carac_obj = [radius, length]
                    
        else:
            type_obj = "box"
            Mdim = np.array([Dx,Dy,Dz])
            Mdim_urdf = np.dot(self.osMpi, Mdim)
            Dx_urdf = abs(Mdim_urdf[0])
            Dy_urdf = abs(Mdim_urdf[1])
            Dz_urdf = abs(Mdim_urdf[2])
            
            carac_obj = [Dx_urdf,Dy_urdf,Dz_urdf]

        return(type_obj,carac_obj)
                   
    def find_mins_maxs(self,obj):
        """!
        
        Reads the .stl file, and get it's minimum and maximum dimensions
        on each of the axes ( of the coordinate system written into the
        .stl file).

        @param obj : Mesh object.
            Object describing the .stl file.

        @return minx, maxx : float.
            Minimum and maximum positions of the points of the .stl file, on 
            the x axis. 
        @return miny, maxy : float.
            Minimum and maximum positions of the points of the .stl file, on 
            the y axis. 
        @return minz, maxz : float.
            Minimum and maximum positions of the points of the .stl file, on 
            the z axis. 

        """
        minx = maxx = miny = maxy = minz = maxz = None
        for p in obj.points:
            # p contains (x, y, z)
            if minx is None:
                minx = p[stl.Dimension.X]
                maxx = p[stl.Dimension.X]
                miny = p[stl.Dimension.Y]
                maxy = p[stl.Dimension.Y]
                minz = p[stl.Dimension.Z]
                maxz = p[stl.Dimension.Z]
            else:
                maxx = max(p[stl.Dimension.X], maxx)
                minx = min(p[stl.Dimension.X], minx)
                maxy = max(p[stl.Dimension.Y], maxy)
                miny = min(p[stl.Dimension.Y], miny)
                maxz = max(p[stl.Dimension.Z], maxz)
                minz = min(p[stl.Dimension.Z], minz)
        return minx, maxx, miny, maxy, minz, maxz

    def writeApproximation(self,name_type,visual_name,f, str_rpy_transform, str_xyz_transform, str_scale_factors,type_obj, carac_obj):
        """!

        Write an approximation of the .stl file as an geometry object supported 
        by the .urdf format, for a tag specified by the variable name_type.
        
        This feature is added for two reasons : 
            * Solve issues with corrupted .stl files, that won't be read afterwards
            during the visualization of the .urdf file.
            
            * Reduce the complexity of calculating model surfaces (for example, 
            to calculate possible collisions of the model with other elements 
            in space).
        
        It won't look as pretty as with a .stl file, but it is nearly sure it 
        will work well.        

        @param name_type : String.
            Type of the tag that should be written into the .urdf file.
            
        @param visual_name : String.
            Name of the .stl file.
            
        @param f : File.
            The .urdf file.
            
        @param str_rpy_transform : String.
            String representing the rpy origin of the .stl object.
            
        @param str_xyz_transform : String.
            String representing the xyz origin of the .stl object.
            
        @param str_scale_factors : String.
            String representing the scale factors of the .stl object.
        
        @param type_obj : String.
            Name of the object approximating the .stl file (cylinder, box, or sphere)
            
        @param carac_obj : List of float.
            Features of the object approximating the .stl file.
            
            Cylinder : [radius of the cylinder, length of the cylinder ].
            
            Sphere : [radius of the sphere].
            
            Box : [Size of box on the x axis, Size of box on y axis, Size of box on z axis].

        @return None.

        """

        f.write(self.ident*" "+ '<'+name_type+  ' name="'+ visual_name+ '"> \n')
        self.ident+= self.ident_size

        f.write(self.ident*" "+ '<origin xyz="'+ str_xyz_transform+  '" rpy="'+ str_rpy_transform+'" /> \n') 
            
        f.write(self.ident*" "+ '<geometry> \n')
        self.ident+= self.ident_size            

        str_obj = '<' + type_obj
        if type_obj == "sphere":
            str_obj += ' radius="' + str(carac_obj[0]) + '" />'
        
        elif type_obj == "cylinder":
            str_obj += ' radius="' + str(carac_obj[0]) + '" length="' + str(carac_obj[1])   + '" />'            
        
        elif type_obj == "box":
            size = "{: .4f} {: .4f} {: .4f}".format( carac_obj[0], carac_obj[1], carac_obj[2] )
            str_obj += ' size="' + size + '" />'               

        f.write(self.ident*" "+ str_obj + ' \n')

        self.ident-= self.ident_size                   
        f.write(self.ident*" "+ "</geometry> \n")
        self.ident-= self.ident_size                   
        f.write(self.ident*" "+ "</"+ name_type +"> \n \n")        
 
        

    def writeExact(self,name_type,visual_name,filename, f, str_rpy_transform, str_xyz_transform, str_scale_factors):
        """!
        Write the exact .stl file object into the .urdf file, for a tag 
        specified by the variable name_type.        

        @param name_type : String.
            Type of the tag that should be written into the .urdf file.
            
        @param visual_name : String.
            Name of the .stl file.
        
        @param filename : String.
            Complete path to the .stl file.
            
        @param f : File.
            The .urdf file.
            
        @param str_rpy_transform : String.
            String representing the rpy origin of the .stl object.
            
        @param str_xyz_transform : String.
            String representing the xyz origin of the .stl object.
            
        @param str_scale_factors : String.
            String representing the scale factors of the .stl object.

        @return None.

        """
        
        f.write(self.ident*" "+ '<'+name_type+  ' name="'+ visual_name+ '"> \n')
        self.ident+= self.ident_size
        
        f.write(self.ident*" "+ '<origin xyz="'+ str_xyz_transform+  '" rpy="'+ str_rpy_transform+'" /> \n') 
        f.write(self.ident*" "+ '<geometry> \n')
        self.ident+= self.ident_size            
        
        f.write(self.ident*" "+ '<mesh filename="package:/'+filename+'" scale="'+ str_scale_factors+ '"/> \n')

        self.ident-= self.ident_size                   
        f.write(self.ident*" "+ "</geometry> \n")
        self.ident-= self.ident_size                   
        f.write(self.ident*" "+ "</"+ name_type +"> \n \n")
            
def readJointSet(root,pymodel):
    """!
    Special case where the .osim file got a "JointSet" tag.
    This means that the definition of joints and the definition of
    body parts of the model are separated. Thus, they need to be treated
    separately. 

    @param root : ElementTree Object.
        Object containing all informations about the .osim file. Those
        informations are organized as a tree.
    
    @param pymodel : Dictionary of List.
        Dictionary containing every useful informations about the .osim
        file, in order to convert this file into a .urdf format.

    @return pymodel : Dictionary of List.
        The pymodel object with the informations into the <JointSet> tag of
        the .osim.

    """
    poss_joints = ['CustomJoint','WeldJoint','PinJoint','UniversalJoint']
    
    for name_joint in poss_joints:
    
        for joint in root.findall('./Model/JointSet/objects/'+name_joint):
        
            joint_data = {
                'name':[], 
                'parent_body':[], 
                'child_body':[],
                'location_in_parent':[], 
                'orientation_in_parent':[], 
                'location':[],
                'orientation':[]
            }
            

            is_socket = type(joint.find('socket_parent_frame')) != type(None) 
            
            if is_socket : 
                # means that we are working with sockets. 
                
                joint_data['name'].append(joint.get('name'))
                
                parent_socket = joint.find('socket_parent_frame').text
                child_socket = joint.find('socket_child_frame').text
                
                frames = joint.findall('frames/PhysicalOffsetFrame')

                for pf in frames:
                    name_pf = pf.get('name')
                    
                    bpart = pf.find("socket_parent").text.split("/")[-1]
                    trans = pf.find("translation").text.split()
                    orient = pf.find("orientation").text.split()
                    
                    # location : Location of the joint in the child body specified in the child reference frame. For SIMM models, this vector is always the zero vector (i.e., the body reference frame coincides with the joint). 
                    # orientation : Orientation of the joint in the owing body specified in the owning body reference frame.  Euler XYZ body-fixed rotation angles are used to express the orientation. 
                
                    # location_in_parent : Location of the joint in the parent body specified in the parent reference frame. Default is (0,0,0).
                    # orientation_in_parent : Orientation of the joint in the parent body specified in the parent reference frame. Euler XYZ body-fixed rotation angles are used to express the orientation. Default is (0,0,0).

                    if name_pf == parent_socket :
                        joint_data['parent_body'].append(bpart)
                        joint_data['location_in_parent'].append(trans)
                        joint_data['orientation_in_parent'].append(orient)
                    elif name_pf == child_socket:
                        joint_data['child_body'].append(bpart)
                        joint_data['location'].append(trans)
                        joint_data['orientation'].append(orient)                       
            
            else:
                print("New case : JointSet but no socket (in readJointSet). Exit.")
                exit(1)

            # Coordinate Set
            coordinates_list = joint.iter('Coordinate')
            coordinate_data = {'name':[],
                               'motion_type':[],
                               'default_value':[],
                               'default_speed_value':[],
                               'range':[],
                               'clamped':[],
                               'locked':[],
                               'prescribed_function':[]
            }
            
            L_coordinate_name = []
            
            for coordinates in coordinates_list:
                L_coordinate_name.append(coordinates.get('name'))
                
                L_coord_dat = list(coordinate_data.keys())
                
                L_get = ['name']
                L_find_split = ['range']
                
                for data in L_coord_dat:
                    if type(coordinates.find(data)) != type(None) :
                        
                        if data in L_get:
                            coordinate_data[data].append(coordinates.get(data) )
                        
                        elif data in L_find_split:
                            coordinate_data[data].append( (coordinates.find(data).text).split() )

                        else:
                            coordinate_data[data].append(coordinates.find(data).text)

            spatial_list = joint.findall('SpatialTransform/TransformAxis')
            spatial_data = {
                'name': [],
                'coordinates': [],
                'axis': []
            }

            if name_joint =='PinJoint':
                # https://simtk.org/api_docs/opensim/api_docs20b/classOpenSim_1_1PinJoint.html
                # z axis of the joint frame
                # --> Unaligned, on z
                #
                
                joint_orientation = np.array(joint_data['orientation'][0],dtype=float)

                M_j_o = alg.euler_matrix(joint_orientation[0],joint_orientation[1], joint_orientation[2])
                
                M_z = M_j_o[:3,2]

                z_orientation = [str(M_z[0]), str(M_z[1]), str(M_z[2])]
                
                spatial_data['name'] = ['rotation1', 'rotation2', 'rotation3', 'translation1', 'translation2', 'translation3']
                
                
                spatial_data['coordinates'] = [L_coordinate_name[-1],  None, None, None, None, None ]
                spatial_data['axis'] = [z_orientation,  ['0', '1', '0'], ['1', '0', '0'], ['1', '0', '0'], 
                                            ['0', '1', '0'], ['0', '0', '1'] ]
                
            elif name_joint =='UniversalJoint':
                # https://simtk.org/api_docs/opensim/api_docs/classOpenSim_1_1UniversalJoint.html#details
                # first one on x axis in the joint frame
                # second one on y axis in the joint frame
                # 
                # --> Unaligned, on x, then Unaligned on y.

                joint_orientation = np.array(joint_data['orientation'][0],dtype=float)
                
                M_j_o = alg.euler_matrix(joint_orientation[0],joint_orientation[1], joint_orientation[2])
                
                M_x = M_j_o[:3,0]
                M_y = M_j_o[:3,1]
                x_orientation = [str(M_x[0]), str(M_x[1]), str(M_x[2])]
                y_orientation = [str(M_y[0]), str(M_y[1]), str(M_y[2])]
                
                spatial_data['name'] = ['rotation1', 'rotation2', 'rotation3', 'translation1', 'translation2', 'translation3']
                spatial_data['coordinates'] = [L_coordinate_name[-2],  L_coordinate_name[-1], None, None, None, None ]
                spatial_data['axis'] = [x_orientation,  y_orientation, ['1', '0', '0'], ['1', '0', '0'], 
                                            ['0', '1', '0'], ['0', '0', '1'] ]            
            
            elif name_joint == "WeldJoint":
                # https://simtk.org/api_docs/opensim/api_docs20b/classOpenSim_1_1WeldJoint.html#_details
                # Weld Joint = fixed joint.

                spatial_data['name'] = ['rotation1', 'rotation2', 'rotation3', 'translation1', 'translation2', 'translation3']
                spatial_data['coordinates'] = [None, None, None, None, None, None ]
                spatial_data['axis'] = [ ['1','0','0'], ['0','1','0'], ['0', '0', '1'], ['1', '0', '0'], 
                                                ['0', '1', '0'], ['0', '0', '1'] ]     
                

            else:

                for spatial_transform in spatial_list:
                    is_linear_function = type(spatial_transform.find('LinearFunction')) != type(None)
                    is_simm_function = type(spatial_transform.find('SimmSpline')) != type(None)

                    if is_linear_function or is_simm_function:
                    
                        spatial_data['coordinates'].append(spatial_transform.find('coordinates').text)
                            
                    else:
                        spatial_data['coordinates'].append(None)

                    spatial_data['axis'].append((spatial_transform.find('axis').text).split())

                    spatial_data['name'].append(spatial_transform.get('name'))      
                    
            pymodel['Joints'].append([joint_data, coordinate_data, spatial_data])    
    return(pymodel)


def readJoint(name_joint,pymodel,bodies,body_name):
    """!
    Read all joints of type "name_joint" belonging to body object "bodies", of
    name "body_name". Add all the useful informations to pymodel.

    @param name_joint : String.
        Type of the considered joints.
        
    @param pymodel : Dictionary of List.
        Dictionary containing every useful informations about the .osim
        file, in order to convert this file into a .urdf format.

    @param bodies : ElementTree.
        Object Element, containing the informations of the selected body.
    @param body_name : String.
        Name of the body part, representing the child body of the joint.

    @return pymodel : Dictionary of List.
        The pymodel object with the informations of the considered joint.
        
    """
    joints_list =  bodies.iter(name_joint)
    n = -1
    for joint in joints_list:
        if n == -1:
            n = 0
        joint_data = {
                'name':[], 
                'parent_body':[], 
                'child_body':[],
                'location_in_parent':[], 
                'orientation_in_parent':[], 
                'location':[],
                'orientation':[]
        }
        joint_data['name'].append(joint.get('name').replace(" ",""))
        joint_data['child_body'].append(body_name.replace(" ",""))
        joint_data['parent_body'].append(joint.find('parent_body').text.replace(" ",""))
        joint_data['location_in_parent'].append((joint.find('location_in_parent').text).split())
        joint_data['orientation_in_parent'].append((joint.find('orientation_in_parent').text).split())
        joint_data['location'].append((joint.find('location').text).split())
        joint_data['orientation'].append((joint.find('orientation').text).split())

        # Coordinate Set
        coordinates_list = joint.iter('Coordinate')
        coordinate_data = {'name':[],
                               'motion_type':[],
                               'default_value':[],
                               'default_speed_value':[],
                               'range':[],
                               'clamped':[],
                               'locked':[],
                               'prescribed_function':[]
        }
            
        L_coordinate_name = []
            
        for coordinates in coordinates_list:
            L_coordinate_name.append(coordinates.get('name').replace(" ",""))
            coordinate_data['name'].append(coordinates.get('name').replace(" ",""))
            coordinate_data['motion_type'].append(coordinates.find('motion_type').text)
            coordinate_data['default_value'].append(coordinates.find('default_value').text)
            coordinate_data['default_speed_value'].append(coordinates.find('default_speed_value').text)
            coordinate_data['range'].append((coordinates.find('range').text).split())
            coordinate_data['clamped'].append(coordinates.find('clamped').text)
            coordinate_data['locked'].append(coordinates.find('locked').text)
            coordinate_data['prescribed_function'].append(coordinates.find('prescribed_function').text)
            
        #get spatial transform
        spatial_list = joint.iter('TransformAxis')
        spatial_data = {
                'name': [],
                'coordinates': [],
                'axis': []
        }
            
        if name_joint =='PinJoint':
            # https://simtk.org/api_docs/opensim/api_docs20b/classOpenSim_1_1PinJoint.html
            # z axis of the joint frame
            # --> Unaligned, on z
             
            joint_orientation = np.array(joint_data['orientation'][0],dtype=float)

            M_j_o = alg.euler_matrix(joint_orientation[0],joint_orientation[1], joint_orientation[2])
            
            M_z = M_j_o[:3,2]

            z_orientation = [str(M_z[0]), str(M_z[1]), str(M_z[2])]

            spatial_data['name'] = ['rotation1', 'rotation2', 'rotation3', 'translation1', 'translation2', 'translation3']
                
                
            spatial_data['coordinates'] = [L_coordinate_name[-1],  None, None, None, None, None ]
            spatial_data['axis'] = [z_orientation,  ['0', '1', '0'], ['1', '0', '0'], ['1', '0', '0'], 
                                            ['0', '1', '0'], ['0', '0', '1'] ]
                
        elif name_joint =='UniversalJoint':
            # https://simtk.org/api_docs/opensim/api_docs/classOpenSim_1_1UniversalJoint.html#details
            # first one on x axis in the joint frame
            # second one on y axis in the joint frame
            # 
            # --> Unaligned, on x, then Unaligned on y.
            #
                
            joint_orientation = np.array(joint_data['orientation'][0],dtype=float)

            M_j_o = alg.euler_matrix(joint_orientation[0],joint_orientation[1], joint_orientation[2])
                
            M_x = M_j_o[:3,0]
            M_y = M_j_o[:3,1]
            x_orientation = [str(M_x[0]), str(M_x[1]), str(M_x[2])]
            y_orientation = [str(M_y[0]), str(M_y[1]), str(M_y[2])]
                
            spatial_data['name'] = ['rotation1', 'rotation2', 'rotation3', 'translation1', 'translation2', 'translation3']
            spatial_data['coordinates'] = [L_coordinate_name[-2],  L_coordinate_name[-1], None, None, None, None ]
            spatial_data['axis'] = [x_orientation,  y_orientation, ['1', '0', '0'], ['1', '0', '0'], 
                                            ['0', '1', '0'], ['0', '0', '1'] ]            
        
        elif name_joint == "WeldJoint":
            # https://simtk.org/api_docs/opensim/api_docs20b/classOpenSim_1_1WeldJoint.html#_details
            # Weld Joint = fixed joint.

            spatial_data['name'] = ['rotation1', 'rotation2', 'rotation3', 'translation1', 'translation2', 'translation3']
            spatial_data['coordinates'] = [None, None, None, None, None, None ]
            spatial_data['axis'] = [ ['1','0','0'], ['0','1','0'], ['0', '0', '1'], ['1', '0', '0'], 
                                            ['0', '1', '0'], ['0', '0', '1'] ]     
            
            
        else:
            # Custom Joint case
            for spatial_transform in spatial_list:
                is_linear_function = type(spatial_transform.find('function/LinearFunction')) != type(None)
                is_simm_function = type(spatial_transform.find('function/SimmSpline')) != type(None)

                if is_linear_function or is_simm_function:
                    text = spatial_transform.find('coordinates').text
                    if type(text) != type(None):
                        spatial_data['coordinates'].append(spatial_transform.find('coordinates').text.replace(" ",""))
                    else:
                        spatial_data['coordinates'].append(None)
                            
                else:
                    spatial_data['coordinates'].append(None)

                spatial_data['axis'].append((spatial_transform.find('axis').text).split())

                spatial_data['name'].append(spatial_transform.get('name').replace(" ",""))
            
            # sort the axis and names in good order (rotation1 -> rotation3; translation1 -> translation3)
            L_name = [" " for k in range(6) ]
            L_axis = [ [] for k in range(6) ]
            D_sort = {'rotation1' : 0, 'rotation2' : 1, 'rotation3' : 2, 'translation1' : 3,'translation2' : 4,'translation3' : 5 }
            for k in range( len(spatial_data['name'])):
                i = D_sort[spatial_data['name'][k] ]
                L_name[i] = spatial_data['name'][k]
                L_axis[i] = spatial_data['axis'][k]
            for k in range(6):
                if L_name[k] == " ":
                    del(L_name[k])
                    del(L_axis[k])
                    k -=1
            spatial_data['name'] = L_name
            spatial_data['axis'] = L_axis
            
            

        pymodel['Joints'].append([joint_data, coordinate_data, spatial_data])    
   
    return(pymodel)

def readOsim(filename):
    """!
    
    First method called to read .osim files.
    it first determines the version of the file, in order to determine 
    which parser should be used.

    @param filename : String.
        Path to the .osim file.

    @return pymodel : Dictionary of List.
        Dictionary containing every useful informations about the .osim
        file, in order to convert this file into a .urdf format.

    """
    tree = xml.parse(filename)
    root = tree.getroot()
    pymodel = {}
    for version in root.findall('.'):
        n_ver = int(version.get('Version'))
    if n_ver == 40000:
        pymodel = readOsim40(filename)
    if n_ver <= 30000:
        pymodel = readOsim30(filename)
    return(pymodel)
        
def readVisuals40(pymodel,bodies):
    """!
    
    Method used to read visual information of a body part "bodies" (linked to 
    the geometry file used to represent the body part : name of the geometry 
    part, scale factors, ...). To do so, this method reads the tags from the 
    body part of the .osim file usually associated with the visual information. 
    
    This method is specific to .osim files of version 4.0 or higher.
    
    @param bodies : ElementTree.
        Object Element, containing the informations of the selected body.
    
    @param pymodel : Dictionary of List.
        Dictionary containing every useful informations about the .osim
        file, in order to convert this file into a .urdf format.

    @return pymodel : Dictionary of List.
        The pymodel object with the visual informations of the body part.

    """
    visuals_data = {
            'scale_factors':[]
        }
        
    bones_data = {
                'geometry_file': [],
                'color': [],
                'transform': [],
                'scale_factors': [],
                'opacity': []
        }

    visuals_data,bones_data = readVisualsName40('components/PhysicalOffsetFrame',visuals_data,bones_data,bodies)        

    visuals_data,bones_data = readVisualsName40('PhysicalOffsetFrame',visuals_data,bones_data,bodies)        
        
    visuals_data,bones_data = readVisualsName40('.',visuals_data,bones_data,bodies)        

    pymodel['Visuals'].append([visuals_data, bones_data])
                
    return(pymodel)


def readVisualsName40(name,visuals_data,bones_data,bodies):
    """!
    
    Method used to read visual informations, written under a tag "name", of 
    a body part "bodies". 
    This method is specific to .osim files of version 4.0 or higher.   

    @param name : String.
        Name of the visual tag currently being analysed.
    
    @param visuals_data : Dictionary of List.
        Dictonnary containing the general visual informations of all geometric
        files of the body part.
        (Sometimes the scale factors is generalised to all geometry_file of the 
         body part. In this case, this information is recorded here. )
    
    @param bones_data : Dictionary of List.
        Dictonnary containing specific visual informations of each geometric 
        files of the body part.
    
    @param bodies : ElementTree.
        Object Element, containing the informations of the selected body.

    @return visuals_data : Dictionary of List.
        The visuals_data Dictionary containing the visual informations of the
        selected body, under the "name" tag.
    
    @return bones_data : Dictionary of List.
        The bones_data Dictionary containing the visual informations of the
        selected body, under the "name" tag.

    """
    # Visible Objects
   
    visible_list =  bodies.findall(name)

    for visuals in visible_list:

        visuals_data['scale_factors'].append((visuals.find('FrameGeometry/scale_factors').text).split())
        
        if name == ".":
            # case where the meshes are treated as fixed to the frame and they share the transform of the frame when visualized
            translation = "0.0 0.0 0.0"
            orientation = "0.0 0.0 0.0"
            
        else:
            translation = visuals.find('translation').text
            orientation = visuals.find('orientation').text
        transform = (orientation + " " + translation).split()

        if name == ".":
            bones_list = visuals.findall("attached_geometry/Mesh")
        else:
            bones_list =  visuals.iter('Mesh')
        for bones in bones_list:
            mesh_name = bones.find('mesh_file').text
            
            if mesh_name != None:
                bones_data['geometry_file'].append(bones.find('mesh_file').text) 
                bones_data['color'].append((bones.find('Appearance/color').text).split())
                bones_data['transform'].append(transform)
                bones_data['scale_factors'].append((bones.find('scale_factors').text).split())
                bones_data['opacity'].append(bones.find('Appearance/opacity').text)

    return(visuals_data,bones_data)


def readOsim40(filename):
    """!
    
    Based on the method readOsim of the Ospi package, this method is improved
    in order to be able to read a larger number of .osim files. 
    This method is specific to .osim files of version 4.0 or higher.   

    @param filename : String.
         Path to the .osim file.

    @return pymodel : Dictionary of List.
        Dictionary containing every useful informations about the .osim
        file, in order to convert this file into a .urdf format.

    """
    pymodel = {
        'Bodies':[], 
        'Joints':[], 
        'Visuals':[],
        'Forces':[],
        'Point':[]
    }
    tree = xml.parse(filename)
    root = tree.getroot()
    
    # *********************************************************
    #                   Get body set
    # *********************************************************
    
    # set ground
    
    body_ground = root.findall('./Model/Ground')
    for ground in body_ground:
        body_data = {'name':[], 
                     'mass':[], 
                     'mass_center':[], 
                     'inertia':[]}        
        body_name= ground.get('name')
        body_data['name'].append(body_name)
        body_data['mass'].append('0.0')
        body_data['mass_center'].append(['0.0','0.0','0.0'])
        Y = [['0.0',
                  '0.0',
                  '0.0'],
                 ['0.0',
                  '0.0',
                  '0.0'],
                 ['0.0',
                  '0.0',
                  '0.0']]        
        body_data['inertia'].append(Y)
        pymodel['Bodies'].append(body_data)        
        
        visuals_data = {
                'scale_factors':[]
            }
        visuals_data['scale_factors'].append((ground.find('FrameGeometry/scale_factors').text).split())
            
        bones_data = {
                'geometry_file': [],
                'color': [],
                'transform': [],
                'scale_factors': [],
                'opacity': []
        }
        bones_list =  ground.iter('Mesh')
        for bones in bones_list:
            bones_data['geometry_file'].append(bones.find('mesh_file').text) 
            bones_data['color'].append((bones.find('Appearance/color').text).split())
            bones_data['transform'].append(["0.0","0.0","0.0","0.0","0.0","0.0"])
            bones_data['scale_factors'].append((bones.find('scale_factors').text).split())
               
            bones_data['opacity'].append(bones.find('Appearance/opacity').text)

        pymodel['Visuals'].append([visuals_data, bones_data])        
        
    # test if there is a JointSet tag
    is_joint_set = (  type(root.find("./Model/JointSet")) != type(None)   )
    if is_joint_set:
        # means that there is a JointSet tag... great.
        pymodel = readJointSet(root,pymodel)

    for bodies in root.findall('./Model/BodySet/objects/Body'):
        body_data = {'name':[], 
                     'mass':[], 
                     'mass_center':[], 
                     'inertia':[]}
        body_name= bodies.get('name')
        
        body_data['name'].append(body_name)
        body_data['mass'].append(bodies.find('mass').text)
        body_data['mass_center'].append((bodies.find('mass_center').text).split())
        
        
        
        try:
            Y = [[bodies.find('inertia_xx').text,
                  bodies.find('inertia_xy').text,
                  bodies.find('inertia_xz').text],
                 [bodies.find('inertia_xy').text,
                  bodies.find('inertia_yy').text,
                  bodies.find('inertia_yz').text],
                 [bodies.find('inertia_xz').text,
                  bodies.find('inertia_yz').text,
                  bodies.find('inertia_zz').text]]
        except Exception:
            # error in 4.0, not the same model for inertia
            #print("Bodies error : ", bodies.get('name'))
            inertia = bodies.find('inertia').text
            L_inertia = inertia.split(" ")
            Y = [[L_inertia[0],
                 L_inertia[3],
                 L_inertia[4]
                     ],
                 [L_inertia[3],
                  L_inertia[1],
                  L_inertia[5]
                     ],
                 [L_inertia[4],
                  L_inertia[5],
                  L_inertia[2]
                     ]
                ]
        body_data['inertia'].append(Y)
        pymodel['Bodies'].append(body_data)

        if not is_joint_set : 
            pymodel = readJoint('CustomJoint',pymodel,bodies,body_name)
            pymodel = readJoint('WeldJoint',pymodel,bodies,body_name)
            pymodel = readJoint('PinJoint',pymodel,bodies,body_name)
            pymodel = readJoint('UniversalJoint',pymodel,bodies,body_name)

        # Visible Objects
        
        pymodel = readVisuals40(pymodel,bodies)        

    # *********************************************************
    #                 Get force set
    # *********************************************************
    
    # Schutte1993Muscle_Deprecated
    for forces in root.findall('./Model/ForceSet/objects/Schutte1993Muscle_Deprecated'):
        force_data = {
            'force_name':[],
            'type':[],
            'max_isometric_force':[],
            'optimal_fiber_length':[]
        }
        force_data['force_name'].append(forces.get('name'))
        force_data['type'].append('Schutte1993Muscle_Deprecated')
        force_data['max_isometric_force'].append(forces.find('max_isometric_force').text)
        force_data['optimal_fiber_length'].append(forces.find('optimal_fiber_length').text)        
        # point set
        points = []
        path_point_list =  forces.iter('PathPoint')
        for point in path_point_list:
            point_data = {'point_name':[], 'location':[], 'body':[]}
            point_data['point_name'].append(point.get('name'))
            point_data['location'].append((point.find('location').text).split())
            try :
                point_data['body'].append(point.find('body').text)    #point_data["body"].append(point_data['body'].append(point.find('body').text) ) 
            except Exception:
                # error in 4.0, not the same model for body
                body_str = (point.find('socket_parent_frame').text).split("/")[-1]
                point_data['body'].append(body_str) 
            points.append(point_data)
        pymodel['Forces'].append([force_data,points]) 
        
    # Thelen2003Muscle
    for forces in root.findall('./Model/ForceSet/objects/Thelen2003Muscle'):
        force_data = {
            'force_name':[],
            'type':[],
            'max_isometric_force':[],
            'optimal_fiber_length':[]
        }
        force_data['force_name'].append(forces.get('name'))
        force_data['type'].append('Thelen2003Muscle')
        force_data['max_isometric_force'].append(forces.find('max_isometric_force').text)
        force_data['optimal_fiber_length'].append(forces.find('optimal_fiber_length').text)          
        # point set
        points = []
        path_point_list =  forces.iter('PathPoint')
        for point in path_point_list:
            point_data = {'point_name':[], 'location':[], 'body':[]}
            point_data['point_name'].append(point.get('name'))
            point_data['location'].append((point.find('location').text).split())
            try :
                point_data['body'].append(point.find('body').text)  
            except Exception:
                # error in 4.0, not the same model for body
                body_str = (point.find('socket_parent_frame').text).split("/")[-1]
                
                point_data['body'].append(body_str) 
            points.append(point_data)
        pymodel['Forces'].append([force_data,points])

    # Millard2012EquilibriumMuscle
    for forces in root.findall('./Model/ForceSet/objects/Millard2012EquilibriumMuscle'):
        force_data = {
            'force_name':[],
            'type':[],
            'max_isometric_force':[],
            'optimal_fiber_length':[]
        }
        force_data['force_name'].append(forces.get('name'))
        force_data['type'].append('Millard2012EquilibriumMuscle')
        force_data['max_isometric_force'].append(forces.find('max_isometric_force').text)
        force_data['optimal_fiber_length'].append(forces.find('optimal_fiber_length').text)          
        # point set
        points = []
        path_point_list =  forces.iter('PathPoint')
        for point in path_point_list:
            point_data = {'point_name':[], 'location':[], 'body':[]}
            point_data['point_name'].append(point.get('name'))
            point_data['location'].append((point.find('location').text).split())
            point_data['body'].append( (point.find('socket_parent_frame').text).split("/")[-1]   )
            points.append(point_data)
        pymodel['Forces'].append([force_data,points])

    return pymodel        


def readOsim30(filename):
    """!
    
    Based on the method readOsim of the Ospi package, this method is improved
    in order to be able to read a larger number of .osim files. 
    This method is specific to .osim files of version 3.0 or lower.   

    @param filename : String.
         Path to the .osim file.

    @return pymodel : Dictionary of List.
        Dictionary containing every useful informations about the .osim
        file, in order to convert this file into a .urdf format.


    """
    pymodel = {
        'Bodies':[], 
        'Joints':[], 
        'Visuals':[],
        'Forces':[],
        'Point':[]
    }
    tree = xml.parse(filename)
    root = tree.getroot()
    
    # *********************************************************
    #                   Get body set
    # *********************************************************
    for bodies in root.findall('./Model/BodySet/objects/Body'):
        body_data = {'name':[], 
                     'mass':[], 
                     'mass_center':[], 
                     'inertia':[]}
        body_name= bodies.get('name').replace(" ","")
        
        body_data['name'].append(body_name)
        body_data['mass'].append(bodies.find('mass').text)
        body_data['mass_center'].append((bodies.find('mass_center').text).split())
        try:
            Y = [[bodies.find('inertia_xx').text,
                  bodies.find('inertia_xy').text,
                  bodies.find('inertia_xz').text],
                 [bodies.find('inertia_xy').text,
                  bodies.find('inertia_yy').text,
                  bodies.find('inertia_yz').text],
                 [bodies.find('inertia_xz').text,
                  bodies.find('inertia_yz').text,
                  bodies.find('inertia_zz').text]]
        except Exception:
            # error in 4.0, not the same model for inertia
            #print("Bodies error : ", bodies.get('name'))
            inertia = bodies.find('inertia').text
            L_inertia = inertia.split(" ")
            Y = [[L_inertia[0],
                 L_inertia[3],
                 L_inertia[4]
                     ],
                 [L_inertia[3],
                  L_inertia[1],
                  L_inertia[5]
                     ],
                 [L_inertia[4],
                  L_inertia[5],
                  L_inertia[2]
                     ]
                ]
        body_data['inertia'].append(Y)
        pymodel['Bodies'].append(body_data)


        pymodel = readJoint('CustomJoint',pymodel,bodies,body_name)
        pymodel = readJoint('WeldJoint',pymodel,bodies,body_name)
        pymodel = readJoint('PinJoint',pymodel,bodies,body_name)
        pymodel = readJoint('UniversalJoint',pymodel,bodies,body_name)

        # Visible Objects
        #visible_list =  bodies.iter('VisibleObject')
        visible_list =  bodies.findall('./VisibleObject')
        is_there_one_element = False
        for visuals in visible_list:
            is_there_one_element= True
            
            is_there_display_geom = True  #type(visuals.find('GeometrySet/objects/DisplayGeometry')) != type(None)
            if is_there_display_geom:
                visuals_data = {
                    'scale_factors':[],
                    'show_axes':[],
                    'display_preference':[]
                }


                # visuals_data['scale_factors'].append((visuals.find('scale_factors').text).split())
                # visuals_data['show_axes'].append(visuals.find('show_axes').text)
                # visuals_data['display_preference'].append(visuals.find('display_preference').text)
                    
                bones_list =  visuals.iter('DisplayGeometry')
                #geometry_list =  visuals.iter('DisplayGeometry')
                bones_data = {
                    'geometry_file': [],
                    'color': [],
                    'transform': [],
                    'scale_factors': [],
                    'display_preference': [],
                    'opacity': []
                }
                n = 0
                for bones in bones_list:
                    bones_data['geometry_file'].append(bones.find('geometry_file').text.replace(" ","")) 
                    bones_data['color'].append((bones.find('color').text).split())
                    bones_data['transform'].append((bones.find('transform').text).split())
                    bones_data['scale_factors'].append((bones.find('scale_factors').text).split())
                    bones_data['display_preference'].append(bones.find('display_preference').text)
                    bones_data['opacity'].append(bones.find('opacity').text)
                    n+=1
                    
                print("n : ", n)
                print("geom file : ", bones_data['geometry_file'])
                try : 
                    visuals_data['scale_factors'].append((visuals.find('scale_factors').text).split())
                    visuals_data['show_axes'].append(visuals.find('show_axes').text)
                    visuals_data['display_preference'].append(visuals.find('display_preference').text)

                except AttributeError:
                    visuals_data['scale_factors'] = [['1.00000000', '1.00000000', '1.00000000']]
                    visuals_data['show_axes'] = ['false']
                    visuals_data['display_preference'] = ['4']

                pymodel['Visuals'].append([visuals_data, bones_data])

        if (is_there_one_element == False):
                bones_data = {
                    'geometry_file': [],
                    'color': [],
                    'transform': [],
                    'scale_factors': [],
                    'display_preference': [],
                    'opacity': []
                }
                visuals_data = {
                    'scale_factors':[],
                    'show_axes':[],
                    'display_preference':[]
                }
                pymodel['Visuals'].append([visuals_data, bones_data])                





    # *********************************************************
    #                 Get force set
    # *********************************************************
    
    # Schutte1993Muscle_Deprecated
    for forces in root.findall('./Model/ForceSet/objects/Schutte1993Muscle_Deprecated'):
        force_data = {
            'force_name':[],
            'type':[],
            'max_isometric_force':[],
            'optimal_fiber_length':[]
        }
        force_data['force_name'].append(forces.get('name').replace(" ",""))
        force_data['type'].append('Schutte1993Muscle_Deprecated')
        force_data['max_isometric_force'].append(forces.find('max_isometric_force').text)
        force_data['optimal_fiber_length'].append(forces.find('optimal_fiber_length').text)        
        # point set
        points = []
        path_point_list =  forces.iter('PathPoint')
        for point in path_point_list:
            point_data = {'point_name':[], 'location':[], 'body':[]}
            point_data['point_name'].append(point.get('name').replace(" ",""))
            point_data['location'].append((point.find('location').text).split())
            point_data['body'].append(point.find('body').text.replace(" ",""))
            points.append(point_data)
        pymodel['Forces'].append([force_data,points]) 
        
    # Thelen2003Muscle
    for forces in root.findall('./Model/ForceSet/objects/Thelen2003Muscle'):
        force_data = {
            'force_name':[],
            'type':[],
            'max_isometric_force':[],
            'optimal_fiber_length':[]
        }
        force_data['force_name'].append(forces.get('name').replace(" ",""))
        force_data['type'].append('Thelen2003Muscle')
        force_data['max_isometric_force'].append(forces.find('max_isometric_force').text)
        force_data['optimal_fiber_length'].append(forces.find('optimal_fiber_length').text)          
        # point set
        points = []
        path_point_list =  forces.iter('PathPoint')
        for point in path_point_list:
            point_data = {'point_name':[], 'location':[], 'body':[]}
            point_data['point_name'].append(point.get('name').replace(" ",""))
            point_data['location'].append((point.find('location').text).split())
            try :
                point_data['body'].append( point.find('body').text.replace(" ","") ) 
            except Exception:
                # error in 4.0, not the same model for body
                body_str = (point.find('socket_parent_frame').text).split("/")[-1]
                body_str.replace(" ","")
                point_data['body'].append(body_str) 
            points.append(point_data)
        pymodel['Forces'].append([force_data,points])

    # Millard2012EquilibriumMuscle
    for forces in root.findall('./Model/ForceSet/objects/Millard2012EquilibriumMuscle'):
        force_data = {
            'force_name':[],
            'type':[],
            'max_isometric_force':[],
            'optimal_fiber_length':[]
        }
        force_data['force_name'].append(forces.get('name').replace(" ",""))
        force_data['type'].append('Millard2012EquilibriumMuscle')
        force_data['max_isometric_force'].append(forces.find('max_isometric_force').text)
        force_data['optimal_fiber_length'].append(forces.find('optimal_fiber_length').text)          
        # point set
        points = []
        path_point_list =  forces.iter('PathPoint')
        for point in path_point_list:
            point_data = {'point_name':[], 'location':[], 'body':[]}
            point_data['point_name'].append(point.get('name').replace(" ",""))
            point_data['location'].append((point.find('location').text).split())
            point_data['body'].append(point.find('body').text.replace(" ",""))
            points.append(point_data)
        pymodel['Forces'].append([force_data,points])

    return pymodel        


def VTP2STL(path_vtp,path_stl):
    reader = vtk.vtkXMLPolyDataReader()
    reader.SetFileName(path_vtp)
    reader.Update()

    stlWriter = vtk.vtkSTLWriter()
    stlWriter.SetFileName(path_stl)
    stlWriter.SetInputConnection(reader.GetOutputPort())
    #stlWriter.SetFileTypeToASCII()
    stlWriter.Write()
        


if __name__ == "__main__":
    
    # --- wholebody.osim
    
    # # Path to the ros package
    # ros_package_path = "/home/auctus/catkin_ws/src/human_control/"
    # # Path to the vtp files
    # vtp_path = ''
    # # The path to the .osim model
    # osim_path = '/home/auctus/ospi2urdf/models/whole_body/'

    # filename = 'wholebody.osim'

    # exactly_collision = False
    # exactly_visual = True

    # # True : condition exactly
    # # False : condition approx

    # correc_stl_dic = {
    #     'hat_spine': {True: [],
    #                   # 0.29422478
    #                   False: [0.00011565, -0.00353864,  0.26422478]
    #                   },
    #     'bofoot': {True: [],
    #                 False: [-0.00004618,  0.025, -0.00304539]
    #                 },
    #     'fingers_r': {True: [np.pi/2., 0., np.pi/2, 0., 0., 0.],
    #                   False: []
    #                   },
    #     'fingers_l': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
    #                   False: []
    #                   },
    #     'jaw': {True: [],
    #             False: [0.00007359,  0.05567701,  0.07865301]
    #             },

    #     'head_and_neck': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
    #                       False: []
    #                       }
    #     #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]

    # }
    
    # --- upper_body.osim
    
    # # Path to the ros package
    # ros_package_path = "/home/auctus/catkin_ws/src/human_control/"
    # # Path to the vtp files
    # vtp_path = ''
    # # The path to the .osim model
    # osim_path = '/home/auctus/ospi2urdf/models/upper_body/'

    # filename = 'full_upper_body_marks.osim'

    # exactly_collision = False
    # exactly_visual = True

    # # True : condition exactly
    # # False : condition approx

    # correc_stl_dic = {
    #     'hat_spine': {True: [],
    #                   # 0.29422478
    #                   False: [0.00011565, -0.00353864,  0.26422478]
    #                   },
    #     'bofoot': {True: [],
    #                 False: [-0.00004618,  0.025, -0.00304539]
    #                 },
    #     'fingers_r': {True: [np.pi/2., 0., np.pi/2, 0., 0., 0.],
    #                   False: []
    #                   },
    #     'fingers_l': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
    #                   False: []
    #                   },
    #     'jaw': {True: [],
    #             False: [0.00007359,  0.05567701,  0.07865301]
    #             },

    #     'head_and_neck': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
    #                       False: []
    #                       }
    #     #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]

    # }

    # --- full_upper_body_generalized.osim
    
    # # Path to the ros package
    # ros_package_path = "/home/auctus/catkin_ws/src/human_control/"
    # # Path to the vtp files
    # vtp_path = ''
    # # The path to the .osim model
    # osim_path = '/home/auctus/ospi2urdf/models/upper_body_generalized/'

    # filename = 'full_upper_body_generalized.osim'

    # exactly_collision = False
    # exactly_visual = True

    # # True : condition exactly
    # # False : condition approx

    # correc_stl_dic = {
    #     'hat_spine': {True: [],
    #                   # 0.29422478
    #                   False: [0.00011565, -0.00353864,  0.26422478]
    #                   },
    #     'bofoot': {True: [],
    #                 False: [-0.00004618,  0.025, -0.00304539]
    #                 },
    #     'fingers_r': {True: [np.pi/2., 0., np.pi/2, 0., 0., 0.],
    #                   False: []
    #                   },
    #     'fingers_l': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
    #                   False: []
    #                   },
    #     'jaw': {True: [],
    #             False: [0.00007359,  0.05567701,  0.07865301]
    #             },

    #     'head_and_neck': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
    #                       False: []
    #                       }
    #     #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]

    # }

    # --- full_upper_body_generalized.osim, scaled by AddBiomechanics
    
    # Path to the ros package
    ros_package_path = "/home/auctus/sensoring_ws/opensim_ws/src/human_control/"
    # Path to the geometry files
    geom_path = "/home/auctus/sensoring_ws/opensim_ws/src/human_control/description/full_upper_body_generalized/stl"
    geom_type = "stl"
    geom_files_props = [geom_path,geom_type]
    # The path to the .osim model
    osim_path = '/home/auctus/sensoring_ws/opensim_ws/src/ospi2urdf2/models/upper_body_expe/'

    filename = 'full_upper_body_marks_mod.osim'

    exactly_collision = False
    exactly_visual = True

    # True : condition exactly
    # False : condition approx

    correc_stl_dic = {
        'hat_spine': {True: [],
                      # 0.29422478
                      False: [0.00011565, -0.00353864,  0.26422478]
                      },
        'bofoot': {True: [],
                    False: [-0.00004618,  0.025, -0.00304539]
                    },
        'fingers_r': {True: [np.pi/2., 0., np.pi/2, 0., 0., 0.],
                      False: []
                      },
        'fingers_l': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
                      False: []
                      },
        'jaw': {True: [],
                False: [0.00007359,  0.05567701,  0.07865301]
                },

        'head_and_neck': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
                          False: []
                          }
        #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]

    }

    # --- two_arms_only.osim
    
    # # Path to the ros package
    # ros_package_path = "/home/auctus/catkin_ws/src/human_control/"
    # # Path to the vtp files
    # vtp_path = ''
    # # The path to the .osim model
    # osim_path = '/home/auctus/ospi2urdf/models/two_arms_only/'

    # filename = 'two_arms_only.osim'

    # exactly_collision = False
    # exactly_visual = True

    # # True : condition exactly
    # # False : condition approx

    # correc_stl_dic = {
    #     'hat_spine': {True: [],
    #                   # 0.29422478
    #                   False: [0.00011565, -0.00353864,  0.26422478]
    #                   },
    #     'bofoot': {True: [],
    #                 False: [-0.00004618,  0.025, -0.00304539]
    #                 },
    #     'fingers_r': {True: [np.pi/2., 0., np.pi/2, 0., 0., 0.],
    #                   False: []
    #                   },
    #     'fingers_l': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
    #                   False: []
    #                   },
    #     'jaw': {True: [],
    #             False: [0.00007359,  0.05567701,  0.07865301]
    #             },

    #     'head_and_neck': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
    #                       False: []
    #                       }
    #     #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]
    # }

    # --- Jing2019.osim
    
    # # Path to the ros package
    # ros_package_path = "/home/auctus/catkin_ws/src/human_control/"
    # # Path to the vtp files
    # vtp_path = "/home/auctus/ospi2urdf/models/Jing2019/Geometry"
    # # The path to the .osim model
    # osim_path = '/home/auctus/ospi2urdf/models/Jing2019/'

    # filename = 'thu1.osim'

    # exactly_collision = False
    # exactly_visual = True

    # # True : condition exactly
    # # False : condition approx

    # correc_stl_dic = {

    #     'bofoot': {True: [],
    #                 False: [-0.00004618,  0.025, -0.00304539]
    #                 }

    #     #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]

    # }    


    # --- arm_swing.osim
    
    # # Path to the ros package
    # ros_package_path = "/home/auctus/catkin_ws/src/human_control/"
    # # Path to the vtp files
    # vtp_path = "/home/auctus/ospi2urdf/models/arm_swing/Geometry"
    # # The path to the .osim model
    # osim_path = '/home/auctus/ospi2urdf/models/arm_swing/'

    # filename = 'Adjusted_ULBmodelOS4_noThelen.osim'

    # exactly_collision = False
    # exactly_visual = True

    # # True : condition exactly
    # # False : condition approx

    # correc_stl_dic = {

    #     'bofoot': {True: [],
    #                 False: [-0.00004618,  0.025, -0.00304539]
    #                 }

    #     #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]

    # }    


    # # --- two_arms_only_marks.osim
    
    # # # Path to the ros package
    # ros_package_path = "/home/auctus/catkin_ws/src/human_control/"
    # # Path to the vtp files
    # vtp_path = ''
    # # The path to the .osim model
    # osim_path = '/home/auctus/ospi2urdf/models/two_arms_only/'

    # filename = 'two_arms_only_marks.osim'

    # exactly_collision = False
    # exactly_visual = True

    # # True : condition exactly
    # # False : condition approx

    # correc_stl_dic = {
    #     'hat_spine': {True: [],
    #                   # 0.29422478
    #                   False: [0.00011565, -0.00353864,  0.26422478]
    #                   },
    #     'bofoot': {True: [],
    #                 False: [-0.00004618,  0.025, -0.00304539]
    #                 },
    #     'fingers_r': {True: [np.pi/2., 0., np.pi/2, 0., 0., 0.],
    #                   False: []
    #                   },
    #     'fingers_l': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
    #                   False: []
    #                   },
    #     'jaw': {True: [],
    #             False: [0.00007359,  0.05567701,  0.07865301]
    #             },

    #     'head_and_neck': {True: [np.pi/2, 0., np.pi/2, 0., 0., 0.],
    #                       False: []
    #                       }
    #     #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]

    # }    

    # # --- arm26.osim

    # # Path to the ros package
    # ros_package_path = "/home/auctus/catkin_ws/src/human_control/"
    # # Path to the vtp files
    # vtp_path='/home/auctus/opensim_optitrack/OpenSim-Gui-Install/Geometry'
    # # The path to the .osim model
    # osim_path = '/home/auctus/ospi2urdf/models/arm26/'
    # filename='arm26.osim'

    # exactly_collision = False
    # exactly_visual = True

    # # True : condition exactly
    # # False : condition approx

    # correc_stl_dic = {
    #     'ground_jaw':{True:[],
    #               False:[0.00067671,  -0.04661849, 0.01257363]
    #               },
    #     'ground_skull':{True:[],
    #               False:[-0.01669981,  -0.03813969, 0.10198523]
    #               },
    #     'arm_r_radius':{True:[],
    #               False:[0.06350344,  0.00224989,  -0.129649]
    #               },

    #       'ground_r_scapula':{True:[],
    #               False:[0.10804415, -0.04549465, -0.01132800]
    #               }

    #     }


    # # --- Rajagopal2015.osim

    # #Path to the ros package
    # ros_package_path = "/home/auctus/catkin_ws/src/human_control/"
    # # Path to the vtp files
    # vtp_path = '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/SimulationDataAndSetupFiles-4.0/Geometry'
    # # The path to the .osim model
    # osim_path = '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/SimulationDataAndSetupFiles-4.0'
    # filename = 'Rajagopal2015.osim'

    # exactly_collision = False
    # exactly_visual = True

    # # True : condition exactly
    # # False : condition approx

    # correc_stl_dic = {
    #     # 'hat_spine':{True:[],
    #     #              False:[0.00011565, -0.00353864,  0.29422478]
    #     #              },
    #     # 'ground_jaw':{True:[],
    #     #           False:[0.00067671,  -0.04661849, 0.01257363]
    #     #           },
    #     # 'ground_skull':{True:[],
    #     #           False:[-0.01669981,  -0.03813969, 0.10198523]
    #     #           },
    #     # 'arm_r_radius':{True:[],
    #     #           False:[0.06350344,  0.00224989,  -0.129649]
    #     #           },

    #     'bofoot': {True: [],
    #                 False: [-0.00004618,  0.025, -0.00304539]
    #                 }
    #     # 'fingers_r':{True:[0.,0.,0.,0.,0.,0.],
    #     #              False:[]
    #     #              },
    #     #  'fingers_l':{True:[0.,0.,0.,0.,0.,0.],
    #     #              False:[]
    #     #              },
    #     #  'head_and_neck':{True:[0.,0.,0.,0.,0.,0.],
    #     #              False:[]neck':{True:[0.,0.,0.,0.,0.,0.],
    #     #              }
    #     #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]

    # }
    
    # # --- TwoArm_7ddOS4.osim
    
    # ### Path to the ros package.
    # ros_package_path = "/home/auctus/catkin_ws/src/human_control/"
    # ### Path to the vtp files
    # vtp_path = '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm7/Geometry'
    # ### The path to the .osim model
    # osim_path =  '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm7'

    # #"/home/auctus/ospi2urdf/models/tst_arm"
    
    # #'/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/Arm7'
    
    # ### Name of the .osim file.
    # filename = 'TwoArm_7ddOS4.osim'

    # ### True : try to represent the collision aspect of the model from the .stl files.
    # ### False : use approximations according to the dimensions of the .stl file.
    # exactly_collision = False
    
    # ### True : try to represent the visual aspect of the model from the .stl files.
    # ### False : use approximations according to the dimensions of the .stl file.
    # exactly_visual = True


    # ### Used to correct the positions of the .stl files.
    # correc_stl_dic = {
    #     # 'hat_spine':{True:[],
    #     #              False:[0.00011565, -0.00353864,  0.29422478]
    #     #              },
    #     # 'ground_jaw':{True:[],
    #     #           False:[0.00067671,  -0.04661849, 0.01257363]
    #     #           },
    #     # 'ground_skull':{True:[],
    #     #           False:[-0.01669981,  -0.03813969, 0.10198523]
    #     #           },
    #     # 'ulna':{True:[0.,0.,0.,0.,0.,0.0],
    #     #           False:[]
    #     #           },
    #     'radius_r':{True:[],
    #               False:[0.01416360,0.01847673,-0.19803363]
    #               },
    #     # 'ulna_l':{True:[0.,0.,0.,0.,0.,0.0],
    #     #           False:[]
    #     #           },
    #     'radius_l':{True:[],
    #               False:[-0.01416360,  0.01847673,  -0.19803363]
    #               },
    #     # "prox_phalanx2_rvs":{True:[],
    #     #           False:[0.00250000,0.00500000, -0.12170212]
    #     #     },
    #     # "mid_phalanx2_rvs":{True:[],
    #     #           False:[0.00250000,  0.00500000, -0.15170212]
    #     #     },
    #     # "distal_phalanx2_rvs":{True:[],
    #     #           False:[0.00250000, 0.00500000, -0.16670212]
    #     #     },        
    #     # 'hand_l':{True:[0.0,0.,0.,0.,0.0,0.05],
    #     #           False:[]
    #     #           }
    #     # 'hand_r':{True:[0.,0.,0.,0.,0.0,0.0],
    #     #           False:[]
    #     #           }
    #     # 'fingers_r':{True:[0.,0.,0.,0.,0.,0.],
    #     #              False:[]
    #     #              },
    #     #  'fingers_l':{True:[0.,0.,0.,0.,0.,0.],
    #     #              False:[]
    #     #              },
    #       'head_and_neck':{True:[0.,0.,0.,0.,0.,0.],
    #                   False:[]
    #                   }
    #     #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]

    # }

    # # --- SubjectMoved.osim
    
    # #Path to the ros package
    # ros_package_path = "/home/auctus/catkin_ws/src/human_control/"
    # # Path to the vtp files
    # vtp_path='/home/auctus/opensim_python/OpenSim/FullBodyModel/Geometry'
    # # The path to the .osim model
    # osim_path = '/home/auctus/opensim_python/OpenSim/FullBodyModel'
    # filename='SubjectMoved.osim'
    
    # exactly_collision = False
    # exactly_visual = True
    
    # # True : condition exactly
    # # False : condition approx
    
    # correc_stl_dic = {
    #     'hat_spine':{True:[],
    #                   False:[0.00011565, -0.00353864,  0.26422478] #0.29422478
    #                   },
    #     'bofoot':{True:[],
    #               False:[-0.00004618,  0.025, -0.00304539]
    #               },
    #     'fingers_r':{True:[0.,0.,0.,0.,0.,0.], 
    #                   False:[]
    #                   },
    #       'fingers_l':{True:[0.,0.,0.,0.,0.,0.], 
    #                   False:[]
    #                   },               
    #       'jaw':{True:[], 
    #               False:[ 0.00007359,  0.05567701,  0.07865301]
    #                   },     
          
    #       'head_and_neck':{True:[0.,0.,0.,0.,0.,0.], 
    #                   False:[]
    #                   }                   
    #     #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]
        
    #     }

    # # --- MoBL_ARMS_module5_scaleIK_mod.osim

    # #Path to the ros package
    # ros_package_path = "/home/auctus/catkin_ws/src/human_control/"
    # # Path to the vtp files
    # vtp_path = '/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/MoBL_ARMS_tutorial_33/MoBL-ARMS_OpenSim_tutorial_33/ModelFiles/Geometry'
    # # The path to the .osim model
    # osim_path ="/home/auctus/Documents/OpenSim/4.2-2020-11-24-d9c8bb2c/Models/MoBL_ARMS_tutorial_33/MoBL-ARMS_OpenSim_tutorial_33/ModelFiles/"
    # filename = 'MoBL_ARMS_module5_scaleIK_mod.osim'

    # exactly_collision = False
    # exactly_visual = True

    # # True : condition exactly
    # # False : condition approx

    # correc_stl_dic = {
    #     # 'hat_spine':{True:[],
    #     #              False:[0.00011565, -0.00353864,  0.29422478]
    #     #              },
    #     # 'ground_jaw':{True:[],
    #     #           False:[0.00067671,  -0.04661849, 0.01257363]
    #     #           },
    #     # 'ground_skull':{True:[],
    #     #           False:[-0.01669981,  -0.03813969, 0.10198523]
    #     #           },
    #     # 'arm_r_radius':{True:[],
    #     #           False:[0.06350344,  0.00224989,  -0.129649]
    #     #           },

    #     'bofoot': {True: [],
    #                 False: [-0.00004618,  0.025, -0.00304539]
    #                 }
    #     # 'fingers_r':{True:[0.,0.,0.,0.,0.,0.],
    #     #              False:[]
    #     #              },
    #     #  'fingers_l':{True:[0.,0.,0.,0.,0.,0.],
    #     #              False:[]
    #     #              },
    #     #  'head_and_neck':{True:[0.,0.,0.,0.,0.,0.],
    #     #              False:[]
    #     #              }
    #     #'hat_ribs_scap':[0.00011565, -0.00353864,  0.37]

    # }



    ### Constructor of the Osim2URDF class.
    o2u  = Osim2URDF(osim_path, geom_files_props, filename, ros_package_path, exactly_visual, exactly_collision,correc_stl_dic,trans_plugin=True,verbose = True,xacro=True,free_flyer=False)
    #M = np.dot(se3.utils.rotate('z', np.pi/2) , se3.utils.rotate('x', np.pi/2) ) 
    #print(M)
    

import sys
sys.path.append('../../ospi')

import subprocess
import time
#from ospi 
#import viewer_utils as vw
import wrapper as wr
#import motion_parser as mtp

import pinocchio as se3
import algebra as alg

## ---Wholebody ---

#The path to the model meshes
mesh_path='/home/auctus/ospi/models/whole_body/obj'
# The path to the model and the filename
filename='/home/auctus/ospi/models/whole_body/wholebody.osim'
# Create a wrapper specific to the whole-body model 
# The wrapper parse the OpenSim model and builds pinocchio model and data
wb_model = wr.Wrapper(filename, mesh_path, name='whole-body_model1')

# --- Arm26 ---

# # The path to the model meshes
# mesh_path='/home/auctus/ospi/models/tst_arm/obj'
# # The path to the model and the filename
# filename='/home/auctus/ospi/models/tst_arm/arm26.osim'
# # Create a wrapper specific to the whole-body model 
# # The wrapper parse the OpenSim model and builds pinocchio model and data
# wb_model = wr.Wrapper(filename, mesh_path, name='arm26')


# TODO : check autres modeles
print(wb_model.model)

model = wb_model.model

data     = wb_model.data

print(len(wb_model.joint_transformations) ) 

# checkout here : https://gepettoweb.laas.fr/doc/stack-of-tasks/pinocchio/topic/doc-v2/doxygen-html/structse3_1_1Model.html#a71ad644ea389fbadfe8831b61554b0a8

# for k in range(len(model.names)):
# #     print(model.names[k])
# #     print( model.joints[k])
# #     print( model.jointPlacements[k])
    
#     print(wb_model.visuals[k][2], wb_model.visuals[k][-1] )

#print(wb_model.forces)
# print(model.upperPositionLimit)
# print(model.lowerPositionLimit)
# print(se3.neutral(model).shape)

#model.saveToXML(model,"tst.xml")
#print(model.effortLimit.shape)
# A = se3.createDatas(model)   
# print(A)


# q= se3.neutral(model)#wb_model.half_sitting()
# q[2] = 0.93

# print('q: %s' % q.T)

# #print(q.shape)
# # Perform the forward kinematics over the kinematic tree
# se3.forwardKinematics(model,data,q)
# # Print out the placement of each joint of the kinematic tree
# for name, oMi in zip(model.names, data.oMi):
#     # print(oMi.translation.T.flat)
#     print(("{:<24} : {: .2f} {: .2f} {: .2f}"
#           .format( name, *oMi.translation.T.flat )))
# print("----- Rotation -----")
# for name, oMi in zip(model.names, data.oMi):
#     print(("{:<24} : {: .2f} {: .2f} {: .2f} "
#             .format(name, *list(alg.euler_from_rot_mat(oMi.rotation)))) )


# for name, oMi in zip(model.names, data.oMi):
#     print(("{:<24} : {: .2f} {: .2f} {: .2f} {: .2f} "
#           .format( name, *oMi.rotation.T.flat )))
